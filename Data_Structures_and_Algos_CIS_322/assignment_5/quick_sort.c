/* Tyler Pereira
* Homework 5 Problem 7.20
* 
* Struggled to compile with zybooks exampes due to  segmentation
* fault errors during recursive calls
*
* Leveraged psuedo code and explanation from https://www.geeksforgeeks.org/quick-sort/
* Provided easy to understand algorithm though it 
* does not use median of 3 partitioning as in the DSAA book example.
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

/* Swap Function */
void swap(int *a, int *b)
{
    int tmp;

    tmp   = *b;
    *b 	= *a;
    *a 	= tmp;
}

int Partition(int arr[], int low, int hi)
{
    /* Pick pivot to be right most eement */
    int pivot = arr[hi];  

    int i = (low - 1); /* set i as smallest index */

    for (int k = low; k <= hi - 1; k++)
    {
        /* if element is < pivot, swap i and k elements */
        if (arr[k] <= pivot)
        {
            i++; /* update i */
            swap(&arr[i], &arr[k]); /* swap i and k */
        }
    }

    /* place pivot between 2 partitions */
    swap(&arr[i + 1], &arr[hi]);

    /* return partition index */
    return (i + 1);
}

void Quicksort(int arr[], int low, int hi)
{
    int j = 0;

    /* If lo > hi then array done sorting */
    if (low < hi) {
         // j is index of partition
         j = Partition(arr, low, hi);

         /* Recursivley sort before Partition */
         Quicksort(arr, low, j - 1);

         /* Recursivley sort after Partition */ 
         Quicksort(arr, j + 1, hi);
    }
} 

void test(int arr[], int size)
{
    printf("Before Sort: ");
    for(int i = 0; i < size; ++i) 
        printf("%d, ", arr[i]);
    printf("\n\n");

    /* begin clock */
    clock_t begin = clock();

    Quicksort(arr, 0, size - 1);

    /* end clock */
    clock_t end = clock();
    double runtime = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Quicksort Runtime: %lf Sec\n\n", runtime);

    printf("After Sort: ");
    for(int i = 0; i < size; ++i)
        printf("%d, ", arr[i]);
    printf("\n\n");
}


int main()
{
    const int size = 100;

    /* 1. Sorted Array */
    printf("a. Sorted Array\n");
    int sorted_arr[size];
    for(int i = 0; i < size - 1; i++)
        sorted_arr[i] = i;
    test(sorted_arr, size);

   
    /* 2. Reversed Array */
    printf("b. Reversed Array\n");
    int reversed_arr[size];
    int j = size - 1;
    for(int i = size - 1; i >= 0; i--)
        reversed_arr[j - i] = i;
    test(reversed_arr, size);

    /* 3. Sorted Array */
    printf("c. Random Array\n");
    int random_arr[size];
    for(int i = 0; i < size - 1; i++)
        random_arr[i] = rand() % 100;
    test(random_arr, size);

	return 0;
}