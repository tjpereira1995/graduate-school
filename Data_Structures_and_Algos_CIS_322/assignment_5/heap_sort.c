/* Tyler Pereira
* Homework 5 Problem 7.11
* 
* Implementing Max Heap as Zybooks provides guidance on max heap heapsort algo
* Implemented recursive heapify method as it was a bit easer to understand for me
* Read through following tutorial aon recursion and heaps https://www.geeksforgeeks.org/heap-sort/
*/

#include <stdio.h>

/* Swap Function */
void swap(int *a, int *b)
{
    int tmp;

    tmp  = *b;
    *b 	= *a;
    *a 	= tmp;
}

/* Heapify array using recursion */
void Recursive_Heapify(int index, int arr[], int size)
{
    /* Init Root and Children */

    /* max is root */
    /* left child is index one element after root */
    /* right child is index two elements after root */
    int max_val = index;
    int left_child = 2*index + 1; 
    int right_child = 2*index + 2; 

    /* If left child > root, max_value equlas left child */
    if ( ((left_child < size) && (arr[left_child] > arr[max_val])) )
       max_val = left_child; 

    /* If right child > root, max_value equlas left child */
    if ( ((right_child < size) && (arr[right_child] > arr[max_val])) )
       max_val = right_child; 

    /* If max_val is not the root element then swap root with new max value */
    if (max_val != index) { 
       swap(&arr[index], &arr[max_val]); 

       /* Recursiivley create Max heap */ 
       Recursive_Heapify(max_val, arr, size); 
    } 
}

void Heapsort(int numbers[], int numbersSize)
{

    /* Build Heap using Heapify Method */
    for (int i = numbersSize / 2 - 1; i >= 0; i--)
        Recursive_Heapify(i, numbers, numbersSize);

    /* print out Initial Heap */
    printf("Intial Heap: ");
    for(int i=0; i<numbersSize;i++)
        printf("%d, ", numbers[i]);
    printf("\n\n");

    /* Begin sorting by swapping max_value with the last index */
    /* After swap is complete, re-run Heapify alogirthim until. we've done this */
    /* On all indexes */
    for (int i = numbersSize - 1; i > 0; i--)
    {
        /* Print out Swaps */
        printf("Swapping %d & %d. Last Index is %d.\n", numbers[0], numbers[i], i );

        /* Print out Heap */
        printf("Heap: ");
        for(int i=0; i<numbersSize;i++)
            printf("%d, ", numbers[i]);
        printf("\n\n");

        swap(&numbers[0], &numbers[i]);

        Recursive_Heapify(0, numbers, i);
    }
}

int main()
{
    int test_arr[12] = { 142, 543, 123, 65, 453, 879, 572, 434, 111, 242, 811, 102 };

    /* Print out Init Array */
    printf("Heap is Empty.\nInitial Array: ");
    for(int i=0; i<12;i++)
        printf("%d, ", test_arr[i]);
    printf("\n\n");

    Heapsort(test_arr, 12);

    /* Print out Final Heap Array */
    printf("Sorted Heap: ");
    for(int i=0; i<12;i++)
        printf("%d, ", test_arr[i]);
    printf("\n\n");

    return 0;
}