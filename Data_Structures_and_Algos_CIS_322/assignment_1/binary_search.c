/* Tyler Pereira
* Homework 1 Problem 2.15
* 
* Leveraging psuedo code from binary search seen in zybooks section 1
* Implementation of runtime calculator leveraged from 
* https://stackoverflow.com/questions/5248915/execution-time-of-c-program
*/

/* Result
* Tylers-MacBook-Air:hmwk1 tjpereira1995$ ./hmwk_2_15 
* Found element and index match.
* Element Index: 3 - Value: 3
* Algorithm Runtime: 0.000072 Sec
*
* Could not find element index equal to element value.
* Algorithm Runtime: 0.000005 Sec
*/

#include <stdio.h>
#include <time.h>

#define NO_ELEMENT_INDEX_EQUAL_TO_VALUE -1

int binary_search(int array_A[], int array_A_size) {

	clock_t begin = clock();

	int mid, low = 0;
	int high = array_A_size - 1;

	while (high >= low) {

		// Find middle element
		mid = (high + low) / 2;
		
		// Start search at middle element
		// If element value is less than element index
		// set low to mid+1 and loop through again
		if (array_A[mid] < mid) {
			low = mid + 1;
		}

		// Start search at middle element
		// If element value is greater than element index
		// set high to mid-1 and loop through again
		else if (array_A[mid] > mid) {
 			high = mid - 1;
		}

		// mid == array_A[mid] so we have a match
		// element value is equal to element index
		else {
			printf("Found element and index match.\nElement Index: %d - Value: %d\n", mid, array_A[mid]);
			
			clock_t end = clock();
			double runtime = (double)(end - begin) / CLOCKS_PER_SEC;
			printf("Algorithm Runtime: %lf Sec\n\n", runtime);
 			
 			return mid;
		}
	}

	printf("Could not find element index equal to element value.\n");

	clock_t end = clock();
	double runtime = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Algorithm Runtime: %lf Sec\n\n", runtime);
	return NO_ELEMENT_INDEX_EQUAL_TO_VALUE; // not found
}


int main()
{

	// Index 3 in A_pass is equal to 3
	int A_pass[10] = {-6,-3,0,3,6,9,12,15,18,21};

	// Node Index equals the value
	int A_fail[10] = {5,10,15,20,25,30,35,40,45,50};

	binary_search(A_pass, 10);
	printf("\n");
	binary_search(A_fail, 10);

	return 0;
}