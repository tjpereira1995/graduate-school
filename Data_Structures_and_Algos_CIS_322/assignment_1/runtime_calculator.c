/* Tyler Pereira
* Homework 1 Problem 2.7 Parts 2, 4, 6
* Implementation of runtime calculator leveraged from 
* https://stackoverflow.com/questions/5248915/execution-time-of-c-program
*/

/* Results
Tylers-MacBook-Air:hmwk1 tjpereira1995$ ./hmwk_2_7 
* 2.7.2) N: 10, Sum:100 Running Time = 0.000003 Sec
* 2.7.4) N: 10, Sum:45 Running Time = 0.000002 Sec
* 2.7.6) N: 10, Sum:870 Running Time = 0.000006 Sec
* 2.7.2) N: 20, Sum:400 Running Time = 0.000004 Sec
* 2.7.4) N: 20, Sum:190 Running Time = 0.000003 Sec
* 2.7.6) N: 20, Sum:16815 Running Time = 0.000070 Sec
* 2.7.2) N: 40, Sum:1600 Running Time = 0.000007 Sec
* 2.7.4) N: 40, Sum:780 Running Time = 0.000004 Sec
* 2.7.6) N: 40, Sum:293930 Running Time = 0.000984 Sec
* 2.7.2) N: 80, Sum:6400 Running Time = 0.000018 Sec
* 2.7.4) N: 80, Sum:3160 Running Time = 0.000009 Sec
* 2.7.6) N: 80, Sum:4909060 Running Time = 0.013439 Sec
* 2.7.2) N: 160, Sum:25600 Running Time = 0.000075 Sec
* 2.7.4) N: 160, Sum:12720 Running Time = 0.000045 Sec
* 2.7.6) N: 160, Sum:80222920 Running Time = 0.195278 Sec
*/

#include <stdio.h>
#include <time.h>

int two(int n){

	clock_t begin = clock();

	int sum = 0;
	int i, j;

	for( i = 0; i < n; ++i )
		for( j = 0; j < n; ++j )
			++sum;
	
	clock_t end = clock();
	double runtime = (double)(end - begin) / CLOCKS_PER_SEC;
	
	printf("2.7.2) N: %d, Sum:%d Running Time = %lf Sec\n", n, sum, runtime);
	return 0;
}

int four(int n){

	clock_t begin = clock();

	int sum = 0;
	int i, j;

	for( i = 0; i < n; ++i )
		for( j = 0; j < i; ++j )
			++sum;
	
	clock_t end = clock();
	double runtime = (double)(end - begin) / CLOCKS_PER_SEC;
	
	printf("2.7.4) N: %d, Sum:%d Running Time = %lf Sec\n", n, sum, runtime);
	return 0;
}

int six(int n){

	clock_t begin = clock();

	int sum = 0;
	int i, j, k;

	for( i = 1; i < n; ++i )
		for( j = 1; j < i * i; ++j )
			if( j % i == 0 )
				for( k = 0; k < j; ++k )
					++sum;
	
	clock_t end = clock();
	double runtime = (double)(end - begin) / CLOCKS_PER_SEC;
	
	printf("2.7.6) N: %d, Sum:%d Running Time = %lf Sec\n", n, sum, runtime);
	return 0;
}


int main()
{
    int i;
	int n[5] = {10,20,40,80,160};
	
	int len_n = sizeof(n) / sizeof(n[0]);

	for(i=0; i<len_n; i++){
		two(n[i]);
		four(n[i]);
		six(n[i]);
	}

   return 0;
}