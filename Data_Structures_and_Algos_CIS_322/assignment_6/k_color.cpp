/* Tyler Pereira
** CIS-322
** Homework 6 Problem 9.39
** Referneces: 
*/
#include <iostream> 
#include <list> 
using namespace std; 

#define COLOR_ERR	-1
#define NO_COLOR 	 0
#define RED 		 1
#define BLUE 		 2

class Graph{
	private:

	public:
        /* number of vertices */
		int num_of_vertices;
		
		/* array of singly linked lists */
		list<int> *adj_list;

		Graph(int verts){
			num_of_vertices = verts;
			adj_list = new list<int>[num_of_vertices];
		}


	void add_edge(int src_index, int dst_index){
		adj_list[src_index].push_back(dst_index);
	}

	int find_degree(int vertex_index) 
	{     
		int degree = 0;                       
		for (int i = 0; i < num_of_vertices; i++)
		{ 
		    for (auto val : adj_list[i]) 
		       if (val == vertex_index){
		     		degree++;
		       }
		} 
	    return degree;          
	} 

	void print_graph() 
	{ 
	    for (int i = 0; i < num_of_vertices; i++)
	    { 
	        cout << "Adjacency list of vertex "
	             << i << ":"; 
	        for (auto val : adj_list[i])
	        	cout << " -> " << val;
	        cout << " -> NULL" << endl; 
	    } 
	}

	int is_two_color_graph() 
	{ 
		int is_two_color  = 1;

		/* init with no color */
		int vertex_color[num_of_vertices];
		for(int i=0; i< num_of_vertices; i++)
			vertex_color[num_of_vertices] = NO_COLOR;

		/* assing first vertex with color */
		vertex_color[0] = RED; 

		/* make safe_color = Blue opposite color assigned */
		int safe_color = BLUE ;

		/* assign colors to remaining vertices */ 
		for (int i = 1; i < num_of_vertices; i++)
		{
			/* find # of incoming edges */ 	
			int indegree = find_degree(i);

			/* if more than 1 incoming edge, graph can not be 2-colored */
			if(indegree > 1){
				cout << "Vertex " << i << " has more than one incoming edge\nTherefore fails test for two-colorability." << endl;
				vertex_color[i] = COLOR_ERR;
				is_two_color = 0;
			} else {
				vertex_color[i] = safe_color;
				safe_color = ((safe_color == RED) ? BLUE : RED); 
			}

		} 

		// print the result 
		for (int i = 0; i < num_of_vertices; i++)
		{
			switch (vertex_color[i])
			{
			    case NO_COLOR: // code to be executed if n = 1;
			    	cout << "Vertex " << i << " -> No Color (" << vertex_color[i] << ")" << endl;
			        break;
			    case RED: 
			    	cout << "Vertex " << i << " -> Red (" << vertex_color[i] << ")" << endl;
			        break;
			    case BLUE: 
			    	cout << "Vertex " << i << " -> Blue (" << vertex_color[i] << ")" << endl;
			        break;
			    default: 
			    	cout << "Vertex " << i << " -> ERR (" << vertex_color[i] << ")" << endl;
			}
		}

		return is_two_color;
	} 
	    
};
 
int main() 
{ 
	Graph g1(5); 
	g1.add_edge(0, 1); 
	g1.add_edge(1, 2);
	g1.add_edge(1, 3);  
	g1.add_edge(2, 3); 
	g1.add_edge(3, 4);
	cout << "\n\n--- Graph 1 --- \n"; 
	g1.print_graph();

	cout << "\n\nTesting Graph 1 for Two-Colorability \n"; 
	int is_two_color = g1.is_two_color_graph(); 

	if(is_two_color)
		cout << "\nGraph Passes Two-Colorability Test\n\n" << endl;
	else
		cout << "\nGraph Fails Two-Colorability Test\n\n" << endl;

	
	Graph g2(5);
	g2.add_edge(0, 1); 
	g2.add_edge(1, 2);
	g2.add_edge(2, 3);   
	g2.add_edge(3, 4);
	cout << "\n\n--- Graph 2 --- \n"; 
	g2.print_graph();

	cout << "\n\nTesting Graph 2 for Two-Colorability \n"; 
	is_two_color = g2.is_two_color_graph(); 

	if(is_two_color)
		cout << "\nGraph Passes Two-Colorability Test\n\n" << endl;
	else
		cout << "\nGraph Fails Two-Colorability Test\n\n" << endl;

	return 0; 
} 
