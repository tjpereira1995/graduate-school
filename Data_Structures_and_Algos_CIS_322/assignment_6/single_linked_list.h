#ifndef SINGLE_LINKED_LIST_H   /* Include guard */
#define SINGLE_LINKED_LIST_H

#include <iostream>

#define ERROR_EMPTY_LIST -1

using namespace std;

struct Node{
	int index;
	int weight;
	struct Node *next;
};

class SingleList{
	private:

	public:
		int  size;
		Node *head;
		Node *tail;

		SingleList(){
			head = nullptr;
			tail = nullptr;
			size = 0;
		}

	/* Member function defintions */

	/* Append - Add Node to end of list 
	** This is the push for a Queue Stucture
	** append will provide FIFO behavior
	*/
	void append(int val, int weight_val){
		/* malloc new node */
		struct Node *new_node = (struct Node*) malloc(sizeof(struct Node)); 

		/* Set index value */
		new_node->index  = val;
		new_node->weight = weight_val;

		/* Since append, point to NULL */
		new_node->next = nullptr;

		/* Check if list is empty.
		** If empty, set new_node as head of list also.
		** Else update current tails next to point to new_node.
		*/
		if(head == nullptr){
			head = new_node;
		} else{
			tail->next = new_node;
		}
		
		/* Always set tail equal to new node */
		tail=new_node;

		/* increment size */
		size++;
	}

	/* Prepend - Add Node to begining of list
	** This is a push for Stack structure
	** Prepend provides LIFO behavior
	*/
	void prepend(int val, int weight_val){
		/* malloc new node */
		struct Node *new_node = (struct Node*) malloc(sizeof(struct Node)); 

		/* Set index value */
		new_node->index  = val;
		new_node->weight = weight_val;

		/* Check if list is empty.
		** If empty, set new_node as tail of list also and point next to Null.
		** Else set new_nodes next to point to current head
		*/
		if(head == nullptr){
			tail = new_node;
			new_node->next = nullptr;
		} else{
			new_node->next = head;
		}
		/* Always set head equal to new node */
		head=new_node;

		/* increment size */
		size++;
	}

	void printListStack(){
		cout << "Current List (Top to Bottom)" << endl;

		/* malloc tmp node */
		struct Node *tmp = (struct Node*) malloc(sizeof(struct Node)); 
		
		/* set tmp node to head */
		tmp=head;

		/* loop through list until tmp = null
		** print index at each node
		*/
		while(tmp != nullptr){
			cout << tmp->index << endl;

			/* Set tmp to next Node */
			tmp = tmp->next;
		}
	}

	void printListQueue(){
		cout << "Current List (Front to Back)" << endl;

		/* malloc tmp node */
		struct Node *tmp = (struct Node*) malloc(sizeof(struct Node)); 
		
		/* set tmp node to head */
		tmp=head;

		/* loop through list until tmp = null
		** print index at each node
		*/
		while(tmp != nullptr){
			cout << tmp->index << endl;
			
			/* Set tmp to next Node */
			tmp = tmp->next;
		}
	}

	void printVertexList(int vertice){
		cout << "Vertice " << vertice << " Adjaceny List: ";

		/* malloc tmp node */
		struct Node *tmp = (struct Node*) malloc(sizeof(struct Node)); 
		
		/* set tmp node to head */
		tmp=head;

		/* loop through list until tmp = null
		** print index at each node
		*/
		while(tmp != nullptr){
			cout << "|" << tmp->index << "|" << tmp->weight << "|" << " -> ";
			
			/* Set tmp to next Node */
			tmp = tmp->next;

		}

		cout << "NUL" << endl;
	}

	void printListIntersection(){
		cout << "Matched/Intersecting Values" << endl;

		/* malloc tmp node */
		struct Node *tmp = (struct Node*) malloc(sizeof(struct Node)); 
		
		/* set tmp node to head */
		tmp=head;

		/* loop through list until tmp = null
		** print index at each node
		*/
		while(tmp != nullptr){
			cout << tmp->index << endl;
			
			/* Set tmp to next Node */
			tmp = tmp->next;
		}
	}

	int isEmpty(){
		return (size == 0) ? 1:0;
	}

	int getLength(){
		return size;
	}


	/* Peek - returns top of stack/front of queue (i.e head value) */
	int peek(){
		if (head ==nullptr){
			cout << "List is empty, cannot peek "  << endl;
			return ERROR_EMPTY_LIST;
		} else {
			cout << "Peek: " << head->index << endl;
			return head->index;
		}
		
	}

	/* Pop - returns and removes top of stack/front of queue (i.e head value) */
	int pop(){
		int pop_val;

		/* If list is empty, return -1
		** Else if node after head is Null then set head back to Null 
		** Else set head to next node
		*/
		if(head == nullptr){
			cout << "List is empty, cannot pop" << endl;
			return ERROR_EMPTY_LIST;
		} else {
			/* Capture top value */
			pop_val = head->index;

			if (head->next == nullptr){
				head = nullptr;
		    } else {
				head = head->next; 
			}
		}

		/* Decrement size */
		size--;
		
		/* Return top value */
		return pop_val;
	}

	int search(int val){
		/* malloc tmp node */
		struct Node *tmp = (struct Node*) malloc(sizeof(struct Node)); 
		
		/* set tmp node to head */
		tmp=head;

		/* loop through list until tmp = null
		** if value to search == Node's index return 1
		*/
		while(tmp != nullptr){
			/* if tmp->index == value return true */
			if(val == tmp->index){
				return 1;
			}

			/* Set tmp to next Node */
			tmp = tmp->next;
		}

		/* return 0 if value not found */
		return 0;
	}


};

#endif