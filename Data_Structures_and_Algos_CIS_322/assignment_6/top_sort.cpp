/* Tyler Pereira
** CIS-322
** Homework 6 Problem 9.1
** Referneces: 9.2 Topologial Sort psuedo code
*/

#include <iostream>
#include <list> 
#include <stack>
#include <queue>

using namespace std;

/* Array to get char from vertex index */
char vertex_index_to_char[11] = {'s', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I','t'};

class Graph{
	private:

	public:
		int num_of_vertices; /* number of vertices */
		list<int> *adj_list; /* array of singly linked lists */

		Graph(int verts)
		{
			num_of_vertices = verts;
			adj_list = new list<int>[num_of_vertices];
		}


	void add_edge(int src_index, int dst_index, int weight)
	{
		adj_list[src_index].push_back(dst_index);
	}

	void print_graph() 
	{ 
	    for (int i = 0; i < num_of_vertices; i++) 
	    { 
	        cout << "Adjacency list of vertex "
	             << i << "[" <<  vertex_index_to_char[i] << "]:"; 
	        for (auto val : adj_list[i]) 
	           cout << " -> " << vertex_index_to_char[val];
	        cout << " -> NULL" << endl; 
	    } 
	} 

	int get_all_edges(){
		int total_edges = 0;
	    for (int i = 0; i < num_of_vertices; i++)
	    {
			for (auto v : adj_list[i])
				total_edges ++;
		}
		return total_edges;
	}

	/*  Returns degree of ver in given graph */
	int findDegree(int vertex_index) 
	{     
		int degree = 0;                       
		for (int i = 0; i < num_of_vertices; i++) 
		{ 
		    for (auto val : adj_list[i]) 
		    {
		    	if (val == vertex_index)
		        {
		     		degree++;
		       	}
		    }

		} 
	    return degree;          
	} 

	void topsort()
	{
		/* list to hold sorted order */
	    list <int> top_order;

	    /* array to hold dgrees for each vertice */
	    int indegree[num_of_vertices];

	    /* get degrees for all vertices */
	    for (int i = 0; i < num_of_vertices; i++){
	    	indegree[i] = findDegree(i);
	    }

	    /* create a queue for all vertices with indegree == 0 */
		queue<int> q; 
		for (int i = 0; i < num_of_vertices; i++) 
			if (indegree[i] == 0) 
				q.push(i); 

		/* Iterate through queue */
	    while(!q.empty()) { 
	        
	        /* Deqeue and push to top_order */
	        int current_vertex = q.front();
	        q.pop(); 
	        top_order.push_back(current_vertex); 
	  
	  		/* iterate through all vertex edge nodes and decrement degree */      
	        for (auto vertex_index : adj_list[current_vertex]) 
	            /* indegree is 0 push to queue */
	            if (--indegree[vertex_index] == 0) 
	                q.push(vertex_index); 

	    } 

	    /* print out top order */
	    cout << "Topologial Sort Order: ";
	    for(auto i = top_order.begin(); i != top_order.end(); ++i)
    		std::cout << vertex_index_to_char[*i] << ' ';
    	cout << "\n" << endl;
	}

	    
};


int main() 
{
	/* ini graph w/ 11 vertexes */
	Graph g(11);

	/* Manually add edges to graph to match Figure 4.81 */
	g.add_edge(0, 1, 1); g.add_edge(0, 4, 4); g.add_edge(0, 7, 6); 	/* sA, sD, sF 	*/
	g.add_edge(1, 2, 2); g.add_edge(1, 5, 2);						/* AB, AE 		*/
	g.add_edge(2, 3, 2);  											/* BC 			*/
	g.add_edge(3, 10 ,4);  											/* Ct 			*/
	g.add_edge(4, 5, 3); g.add_edge(4, 1, 3);						/* DE 			*/
	g.add_edge(5, 3, 2); g.add_edge(5, 6, 3); g.add_edge(5, 9, 3); 	/* EC, EF, EI 	*/
	g.add_edge(6, 3, 1); g.add_edge(6, 10, 3);  					/* FC, Ft 		*/
	g.add_edge(7, 4, 2); g.add_edge(7, 5, 1); g.add_edge(7, 8, 6); 	/* GD, GE, GH 	*/
	g.add_edge(8, 5, 2); g.add_edge(8, 9, 6);  						/* HE, HI 		*/
	g.add_edge(9, 6, 1); g.add_edge(9, 10, 4);  					/* IF, It 		*/

	/* Print initial graph */
	g.print_graph();

	/* Top Sort Graph */
	g.topsort();
    
    return 0;
}
