/* Tyler Pereira
** CIS-322
** Homework 7 Problem 1 b
** Referneces: Zybooks 6.9 dijkstra algo psuedo code
*/

#include <iostream>
#include <list> 
#include <stack>
#include <queue>
#include <limits>

using namespace std;

/* Array to get char from vertex index */
char vertex_index_to_char[7] = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
int INF = std::numeric_limits<int>::max();

class Graph{
   private:

   public:
      int num_of_vertices; /* number of vertices */
      list<int> *adj_list; /* array of singly linked lists */

      /* Manually add edges to graph to match Figure 9.82 */
      int vertice_weights[7][7] = {
         { 0, 5, 3, 0, 0, 0, 0 }, /* A - 0 */
         { 0, 0, 2, 0, 3, 0, 1 }, /* B - 1 */
         { 0, 0, 0, 7, 7, 0, 0 }, /* C - 2 */
         { 2, 0, 0, 0, 0, 6, 0 }, /* D - 3 */
         { 0, 0, 0, 0, 0, 1, 0 }, /* E - 4 */
         { 0, 0, 0, 0, 0, 0, 0 }, /* F - 5 */
         { 0, 0, 0, 0, 1, 0, 0 }  /* G - 6 */
           
      };

      Graph(int verts){
         num_of_vertices = verts;
         adj_list = new list<int>[num_of_vertices];
      }

   void add_edge(int src_index, int dst_index){
      adj_list[src_index].push_back(dst_index);
   }

   void print_graph() 
   { 
       for (int i = 0; i < num_of_vertices; i++) { 
           cout << "Adjacency list of vertex "
                << i << "[" <<  vertex_index_to_char[i] << "]:"; 
           for (auto val : adj_list[i]) 
              cout << " -> " << vertex_index_to_char[val];
           cout << " -> NULL" << endl; 
       } 
   }

   void BellmanFord( int start, int end )
   {
      list<int> unvisitedQueue;

      int dist[num_of_vertices];
      int prev[num_of_vertices];

      /* init dist and prev array */
      for(int i = 0; i < num_of_vertices; i++)  {
         dist[i] = INF;
         prev[i] = 0;
         unvisitedQueue.push_back(i);
      } 

       /* list of Shortest Path */
       list<int> path; bool stop = false;

      /* init start dist to 0 */
       dist[start] = 0;

       path.push_back(start);

      for(int i = start; i < num_of_vertices; i++) { // Main iterations
            int currentV = i ;
            for (int adjV : adj_list[i]) {
               int edgeWeight = vertice_weights[currentV][adjV];
               int alternativePathDistance = dist[currentV] + edgeWeight;
                     
               // If shorter path from startV to adjV is found,
               // update adjV's distance and predecessor
               if (alternativePathDistance < dist[adjV]) {
                  dist[adjV] = alternativePathDistance;
                  prev[adjV] = currentV;
                  if(path.back() != currentV && !stop)
                     path.push_back(currentV);
               }

               /* If we reach destination break */
              if (adjV == end && !stop){
               path.push_back(adjV);
               stop = true;
              }
            }
         }

      cout << "Vertex " << vertex_index_to_char[end] << " Dist: " << dist[end] 
                  << "/Prev: " << vertex_index_to_char[prev[end]] << endl;

      cout << "Shortest Unweighted Path from " << vertex_index_to_char[start] 
      << " to " << vertex_index_to_char[end] << ": ";
      for (int val : path)
         cout << vertex_index_to_char[val] << " -> " ;
      cout << endl; cout << endl; 
   }
   
};




int main() 
{
   /* ini graph w/ 11 vertexes */
   Graph g(7);

   for(int i = 0;  i < 7; i++)
      for(int j = 0;  j < 7; j++)
         if(g.vertice_weights[i][j] > 0 )
            g.add_edge(i, j);

   /* Print initial graph */
   g.print_graph(); cout << "\n\n";

   g.BellmanFord(0, 1); /* A to B */
   g.BellmanFord(0, 2); /* A to C */
   g.BellmanFord(0, 3); /* A to D */
   g.BellmanFord(0, 4); /* A to E */
   g.BellmanFord(0, 5); /* A to F */
   g.BellmanFord(0, 6); /* A to G */

    
    return 0;
}
