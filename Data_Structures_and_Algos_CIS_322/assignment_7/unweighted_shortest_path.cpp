/* Tyler Pereira
** CIS-322
** Homework 6 Problem 9.1
** Referneces: Mixed Zybooks 6.5 and algo in 9.3.1
*/

#include <iostream>
#include <list> 
#include <stack>
#include <queue>
#include <limits>

using namespace std;

/* Array to get char from vertex index */
char vertex_index_to_char[7] = {'B', 'C', 'D', 'E', 'F', 'G', 'A'};
int INF = std::numeric_limits<int>::max();

class Graph{
	private:

	public:
		int num_of_vertices; /* number of vertices */
		list<int> *adj_list; /* array of singly linked lists */

		/* Manually add edges to graph to match Figure 9.82 */
		int vertice_weights[7][7] = {
			{ 0, 2, 0, 3, 0, 1, 0}, /* B - 0 */
			{ 0, 0, 7, 7, 0, 0, 0}, /* C - 1 */
			{ 0, 0, 0, 0, 6, 0, 2}, /* D - 2 */
			{ 0, 0, 0, 0, 1, 0, 0}, /* E - 3 */
			{ 0, 0, 0, 0, 0, 0, 0}, /* F - 4 */
			{ 0, 0, 0, 1, 0, 0, 0}, /* G - 5 */
			{ 5, 3, 0, 0, 0, 0, 0}  /* A - 6 */
		};

		Graph(int verts)
		{
			num_of_vertices = verts;
			adj_list = new list<int>[num_of_vertices];
		}

	void add_edge(int src_index, int dst_index)
	{
		adj_list[src_index].push_back(dst_index);
	}

	void print_graph() 
	{ 
	    for (int i = 0; i < num_of_vertices; i++)
	    { 
	        cout << "Adjacency list of vertex "
	             << i << "[" <<  vertex_index_to_char[i] << "]:"; 
	        for (auto val : adj_list[i]) 
	           cout << " -> " << vertex_index_to_char[val];
	        cout << " -> NULL" << endl; 
	    } 
	}

	void unweighted(int start, int end) 
	{ 

		static int real_start = start;
	    /* Create bool array of known and sett all to false */
	    bool *known = new bool[num_of_vertices]; 
	    for(int i = 0; i < num_of_vertices; i++) 
	        known[i] = false; 

	    bool stop = false;
	  
	    /* Create a queue for BFS */
	    list<int> frontier_queue; 

	    /* list of Shortest Path */
	    list<int> path;
	  
	    /* init starting node */
	    known[start] = true; 
	    frontier_queue.push_back(start); 
	  
	    while(!frontier_queue.empty()) 
	    { 
	        /* deque vertex and add to path */
	        start = frontier_queue.front(); 
	        path.push_back(start);
	        
	        frontier_queue.pop_front(); 

	        /* Loop through adjacent vertices and if it is not known */
	        /* Set it to known and add it to path */
	        for (auto val : adj_list[start])  
	        { 
	        	
	            if (!known[val]) 
	            {
	                known[val] = true; 
	                frontier_queue.push_back(val); 
	            } 

	          	/* If we reach destination break */
		        if (val == end){
		        	path.push_back(val);
		        	stop = true;
		            break;
		        }
		        
	        }

	        if (stop)
            	break;
	    }

	    cout << "Shortest Unweighted Path from " << vertex_index_to_char[real_start] 
	    	<< " to " << vertex_index_to_char[end] << ": ";
		for (int val : path)
			cout << vertex_index_to_char[val] << " -> " ;
		cout << endl; 
	} 



};



int main() 
{
	/* ini graph w/ 11 vertexes */
	Graph g(7);

	for(int i = 0;  i < 7; i++)
		for(int j = 0;  j < 7; j++)
			if(g.vertice_weights[i][j] > 0 )
				g.add_edge(i, j);


	/* Print initial graph */
	g.print_graph(); cout << "\n\n";

	/* BFS for unweighted shortest path */
	g.unweighted(0, 1); /* B - C */
	g.unweighted(0, 2); /* B - D */
	g.unweighted(0, 3); /* B - E */
	g.unweighted(0, 4); /* B - F */
	g.unweighted(0, 5); /* B - G */
	g.unweighted(0, 6); /* B - A */


    
    return 0;
}
