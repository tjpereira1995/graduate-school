/* Tyler Pereira
** CIS-322
** Homework 7 Problem 4
** Referneces: Zybooks 6.13 psuedo code
*/

#include <iostream>
#include <list> 
#include <stack>
#include <queue>
#include <limits>

using namespace std;

/* Array to get char from vertex index */
char vertex_index_to_char[7] = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
int INF = 9999;

class Graph{
   private:

   public:
      int num_of_vertices; /* number of vertices */
      list<int> *adj_list; /* array of singly linked lists */

      /* Manually add edges to graph to match Figure 9.82 */
      int vertice_weights[7][7] = {
         { 0, 5, 3, 0, 0, 0, 0 }, /* A - 0 */
         { 0, 0, 2, 0, 3, 0, 1 }, /* B - 1 */
         { 0, 0, 0, 7, 7, 0, 0 }, /* C - 2 */
         { 2, 0, 0, 0, 0, 6, 0 }, /* D - 3 */
         { 0, 0, 0, 0, 0, 1, 0 }, /* E - 4 */
         { 0, 0, 0, 0, 0, 0, 0 }, /* F - 5 */
         { 0, 0, 0, 0, 1, 0, 0 }  /* G - 6 */
           
      };

      Graph(int verts){
         num_of_vertices = verts;
         adj_list = new list<int>[num_of_vertices];
      }

   void add_edge(int src_index, int dst_index){
      adj_list[src_index].push_back(dst_index);
   }

   void print_graph() 
   { 
       for (int i = 0; i < num_of_vertices; i++) { 
           cout << "Adjacency list of vertex "
                << i << "[" <<  vertex_index_to_char[i] << "]:"; 
           for (auto val : adj_list[i]) 
              cout << " -> " << vertex_index_to_char[val];
           cout << " -> NULL" << endl; 
       } 
   }

   void FloydWarshallAllPairsShortestPath() {
      int numVertices = num_of_vertices;
      // distMatrix is a numVertices x numVertices matrix
      int distMatrix[7][7];
      for(int i = 0; i < num_of_vertices; i++)
         for(int j = 0; j < num_of_vertices; j++)
            distMatrix[i][j] = INF;      
      
      for(int i = 0; i < num_of_vertices; i++){
         distMatrix[i][i] = 0;
         for (int adjV : adj_list[i])
            distMatrix[i][adjV] = vertice_weights[i][adjV];
      }

      cout << "All Pairs Shortest Path Matrix Prior to Algorithm" << endl;
      for(int i = 0; i < num_of_vertices; i++)
         cout << i << " ";
      cout << "\n----------------" << endl;

      for(int i = 0; i < num_of_vertices; i++){
         for(int j = 0; j < num_of_vertices; j++){
            if(distMatrix[i][j] == INF)
               cout << "∞ " ;
            else
               cout << distMatrix[i][j] << " " ;
         }
         cout << "\n";       
      }

      for (int k = 0; k < numVertices; k++) {
         for (int toIndex = 0; toIndex < numVertices; toIndex++) {
            for (int fromIndex = 0; fromIndex < numVertices; fromIndex++) {
               int currentLength = distMatrix[fromIndex][toIndex];
               int possibleLength = distMatrix[fromIndex][k] + distMatrix[k][toIndex];
               if (possibleLength < currentLength)
                  distMatrix[fromIndex][toIndex] = possibleLength;
            }
         }
      }

      cout << "\n"; 

      cout << "All Pairs Shortest Path Matrix After running Algorithm" << endl;
      for(int i = 0; i < num_of_vertices; i++)
         cout << i << " ";
      cout << "\n----------------" << endl;

      
      for(int i = 0; i < num_of_vertices; i++){
         for(int j = 0; j < num_of_vertices; j++){
            if(distMatrix[i][j] == INF)
               cout << "∞ " ;
            else
               cout << distMatrix[i][j] << " " ;
         }
         cout << "\n";       
      }


   }
};


int main() 
{
   /* ini graph w/ 11 vertexes */
   Graph g(7);

   for(int i = 0;  i < 7; i++)
      for(int j = 0;  j < 7; j++)
         if(g.vertice_weights[i][j] > 0 )
            g.add_edge(i, j);

   /* Print initial graph */
   g.print_graph(); cout << "\n\n";

   g.FloydWarshallAllPairsShortestPath();
    
    return 0;
}
