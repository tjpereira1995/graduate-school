/* Tyler Pereira
** CIS-322
** Homework 2 Problem 3.31
** Referneces: Zybooks Chpt 2, DSAA Chpt 3 
*/

/* Results
** C:\Users\pereit3\Documents\school\grad-school\CIS_322\hmwk2>stack_example.exe
** List is empty, cannot pop
** List is empty, cannot peek
** Current List (Top to Bottom)
** 12
** 8
** 5
** 1
** Popped: 12
** Current List (Top to Bottom)
** 8
** 5
** 1
** Peek: 8
*/

#include <iostream>
#include "single_linked_list.h"

using namespace std;

/* Stack Visual Analogy - Cafe Trays
** As you stack trays, people take from the top
** The first tray that began stack is last to be taken
** This is a LIFO real-life example
*/ 

/* Stack Push = List Prepend */
void push(SingleList *list, int val){ list->prepend(val); }

int main() 
{
	/* Init list via Defualt Constructor */
	SingleList *list = new SingleList(); 
	
	/* Try to pop empty list */
	list->pop();

	/* Try to peek empty list */
	list->peek();
	
	/* Push values onto stack */
	push(list,1);
	push(list,5);
	push(list,8);
	push(list,12);

	/* Stack now looks like 
	** |Head|		   |Tail|
	** |12|->|8|->|5|->|1|-> NULL
	** |Top| 		   |Bottom|
	**
	*/
	
	/* Print Stack */
    list->printListStack();

    /* Pop head/top, 12 in this case */
	cout << "Popped: " << list->pop() << endl;

    /* Print Stack */
    list->printListStack();

    /* Peek head/top, 8 in this case */
    list->peek();
    
    return 0;
}
