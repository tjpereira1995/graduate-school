/* Tyler Pereira
** CIS-322
** Homework 2 Problem 3.22
** Referneces: Zybooks Chpt 2, DSAA Chpt 3 
** Converting char to Int (line 38) - https://stackoverflow.com/questions/628761/convert-a-character-digit-to-the-corresponding-integer-in-c
** Checking that char is digit - http://www.cplusplus.com/reference/cctype/isdigit/
*/

/* Results
** C:\Users\pereit3\Documents\school\grad-school\CIS_322\hmwk2>postfix.exe
** Postfix Expression: 6523+8*+3+*
** Postfix Calculation = 288
*/ 

#include <iostream>
#include "single_linked_list.h"

using namespace std;

/* Stack Push = List Prepend */
void push(SingleList *list, int val){ list->prepend(val); }


/* postfixCalculation(char *expression, SingleList *list)
** Description -  Calculate postfix given a postfix expression and linked list
** Parameters
**	- SingleList *expression - postfix expression
**	- SingleList *list - pointer to linked list that will be used as frameowrk of stack
** Return - pop of final value in stack which is caluclation of postfix expression
**
** Big O = O(N)
*/

int postfixCalculation(char *expression, SingleList *list){
	/* iterate through postfix expression */
	int i = 0;
	while(expression[i] != '\0'){

		/* If char is a digit push it onto the stack */
		if(isdigit(expression[i])){
			push(list, expression[i] - '0');
		} else { /* If operator act on mathemetic operator */

			/* Check that stack has at least 2 nodes */
			if(list->size >= 2){

				/* Pop top two Node Values */
				int pop1 = list->pop();
				int pop2 = list->pop(); 

				/* Switch on Math expression
				** Do math operation on two popped values
				** Push result to stack
				*/
				switch(expression[i]){
					case '+':
						push(list, pop1+pop2);
						break;
					case '-':
						push(list, pop1-pop2);
						break;
					case '*':
						push(list, pop1*pop2);
						break;
					case '/':
						push(list, pop1/pop2);
						break;
				}
			}

		}

		/* Increment i */
		i++;

	}

	/* Pop last value in stack and return value
	** This will be the postfix expression result
	*/

	return list->pop();
}


int main() 
{
	/* Init list1 via Defualt Constructor */
	SingleList *list = new SingleList(); 

	/* Using postfix expression from DSAA book */
	char postfix_expression[] = "6523+8*+3+*";

	cout << "Postfix Expression: " << postfix_expression << endl;
	cout << "Postfix Calculation = " << postfixCalculation(postfix_expression, list) << endl;

    return 0;
}
