/* Tyler Pereira
** CIS-322
** Homework 2 Problem 3.4
** Referneces: Zybooks Chpt 2, DSAA Chpt 3 
*/

/* Results
** C:\Users\pereit3\Documents\school\grad-school\CIS_322\hmwk2>intersection.exe
** Matched/Intersecting Values
** 0
** 3
** 8
** 13
*/ 

#include <iostream>
#include "single_linked_list.h"

using namespace std;

/* Notes, L1 ∩ L2 reads as L1 intersect L2
** Means what values are same in L1 and L2
** Can't use binary search even though list is sorted
** Since Linked List is not direct accesible
** Need nested loop through lists
** Big O will be at worst O(N^2)
** Since lists are sorted runtime may be better 
** if search finds value in middle
*/

/* findIntersection(SingleList *l1, SingleList *l2, SingleList *intersection)
** Description -  Finds Node's w/ same Data value and appends data to intersection list
** Parameters
**	- SingleList *l1 - pointer to linked list 1
**	- SingleList *l2 - pointer to linked list 2
**	- SingleList *intersection - pointer to linked list intersection which holds common values between l1 and l2
*/

void findIntersection(SingleList *l1, SingleList *l2, SingleList *intersection){
	/* malloc tmp node */
	struct Node *tmp = (struct Node*) malloc(sizeof(struct Node)); 

	/* Set tmp node to head of list1 */
	tmp = l1->head;

	/* Iterate through list1 */
	while(tmp != nullptr){
		/* Search list2 for tmp->data or
		** list1's node's data
		** If search finds value, append to intersection list
		*/ 
		if(l2->search(tmp->data) == 1){
			intersection->append(tmp->data);
		}

		/* Set tmp to next Node */
		tmp = tmp->next;
	}
}

int main() 
{
	/* Init list1 via Defualt Constructor */
	SingleList *list1 = new SingleList(); 

	/* Populate list1 */
	list1->append(0);  list1->append(3);
	list1->append(5);  list1->append(8);
	list1->append(10); list1->append(13);
	list1->append(15); list1->append(18);

	/* Init list2 via Defualt Constructor */
	SingleList *list2 = new SingleList(); 

	/* Populate list2 */
	list2->append(0);  list2->append(3);
	list2->append(7);  list2->append(8);
	list2->append(9); list2->append(13);
	list2->append(11); list2->append(20);

	/* Intersection should return 0, 3, 8, 13 */
	SingleList *intersection = new SingleList(); 
	findIntersection(list1, list2, intersection);

	/* Print intersection list */
	intersection->printListIntersection();
    
    return 0;
}
