/* Tyler Pereira
** CIS-322
** Homework 2 Problem 3.32
** Referneces: Zybooks Chpt 2, DSAA Chpt 3 
*/

/* Results
** C:\Users\pereit3\Documents\school\grad-school\CIS_322\hmwk2>queue_example.exe
** List is empty, cannot pop
** List is empty, cannot peek
** Current List (Front to Back)
** 1
** 5
** 8
** 12
** Popped: 1
** Current List (Front to Back)
** 5
** 8
** 12
** Peek: 5
*/ 

#include <iostream>
#include "single_linked_list.h"

using namespace std;

/* Queue Visual Analogy - Grocery Line
** As you get in line to checkout
** People at front of line checkout before people at back
** People joininG a line are being appended at the back of the line
** This is a FIFO real-life example
*/ 

/* Queue Push = List Append */
void push(SingleList *list, int val){ list->append(val); }

int main() 
{
	/* Init list via Defualt Constructor */
	SingleList *list = new SingleList(); 
	
	/* Try to pop empty list */
	list->pop();

	/* Try to peek empty list */
	list->peek();
	
	/* Push values onto Queue */
	push(list,1);
	push(list,5);
	push(list,8);
	push(list,12);

	/* Queue now looks like 
	** |Head|		   |Tail|
	** |11|->|5|->|8|->|12|-> NULL
	** |Top| 		   |Bottom|
	**
	*/
	
	/* Print Queue */
    list->printListQueue();

    /* Pop head/front, 1 in this case */
	cout << "Popped: " << list->pop() << endl;

    /* Print Queue */
    list->printListQueue();

    /* Peek head/front, 5 in this case */
    list->peek();
    
    return 0;
}
