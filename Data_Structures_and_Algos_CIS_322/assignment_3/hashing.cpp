/* Tyler Pereira
** CIS-322
** Homework 3 Problem 5.1
** Referneces: memset http://www.cplusplus.com/reference/cstring/memset/
*/

/* Results
Tylers-MacBook-Air:hmwk3 tjpereira1995$ ./hash
--- Inserting Dummy Data into Simple Has ---
Inserting Hash Bucket: 1 Hash Value: 1
Inserting Hash Bucket: 5 Hash Value: 15
Inserting Hash Bucket: 2 Hash Value: 12
Inserting Hash Bucket: 4 Hash Value: 24
Inserting Hash Bucket: 3 Hash Value: 33
Inserting Hash Bucket: 9 Hash Value: 39
Inserting Hash Bucket: 0 Hash Value: 40
Inserting Hash Bucket: 7 Hash Value: 77
Inserting Hash Bucket: 6 Hash Value: 56
Inserting Hash Bucket: 8 Hash Value: 18
Found Hash Bucket: 4 Hash Value: 24
Removing Hash Bucket: 4 Hash Value: 24
--- Simple Hash Table ---
Hash Bucket: 0 Hash Value: 40
Hash Bucket: 1 Hash Value: 1
Hash Bucket: 2 Hash Value: 12
Hash Bucket: 3 Hash Value: 33
Hash Bucket: 4 Hash Value: -1
Hash Bucket: 5 Hash Value: 15
Hash Bucket: 6 Hash Value: 56
Hash Bucket: 7 Hash Value: 77
Hash Bucket: 8 Hash Value: 18
Hash Bucket: 9 Hash Value: 39
--- 5.1.a) Inserting Book Data into Chain Hash Table ---
Inserting Hash Bucket: 1 Hash Value: 4371
Inserting Hash Bucket: 3 Hash Value: 1323
Inserting Hash Bucket: 3 Hash Value: 6173
Inserting Hash Bucket: 9 Hash Value: 4199
Inserting Hash Bucket: 4 Hash Value: 4344
Inserting Hash Bucket: 9 Hash Value: 9679
Inserting Hash Bucket: 9 Hash Value: 1989
--- 5.1.a) Chaining Hash Table ---
Hash Bucket: 0 None
Hash Bucket: 1 None -> 4371
Hash Bucket: 2 None
Hash Bucket: 3 None -> 1323 -> 6173
Hash Bucket: 4 None -> 4344
Hash Bucket: 5 None
Hash Bucket: 6 None
Hash Bucket: 7 None
Hash Bucket: 8 None
Hash Bucket: 9 None -> 4199 -> 9679 -> 1989
--- 5.1.b) Inserting Book Data into Linear Probing Hash Table ---
Inserting Hash Bucket: 1 Hash Value: 4371
Inserting Hash Bucket: 3 Hash Value: 1323
Inserting Hash Bucket: 4 Hash Value: 6173
Inserting Hash Bucket: 9 Hash Value: 4199
Inserting Hash Bucket: 5 Hash Value: 4344
Inserting Hash Bucket: 0 Hash Value: 9679
Inserting Hash Bucket: 2 Hash Value: 1989
--- 5.1.b) Linear Probing Hash Table ---
Hash Bucket: 0 Hash Value: 9679
Hash Bucket: 1 Hash Value: 4371
Hash Bucket: 2 Hash Value: 1989
Hash Bucket: 3 Hash Value: 1323
Hash Bucket: 4 Hash Value: 6173
Hash Bucket: 5 Hash Value: 4344
Hash Bucket: 6 Hash Value: 0
Hash Bucket: 7 Hash Value: 0
Hash Bucket: 8 Hash Value: 0
Hash Bucket: 9 Hash Value: 4199
--- 5.1.b) Inserting Book Data into Quadratic Probing Hash Table ---
Inserting Hash Bucket: 1 Hash Value: 4371
Inserting Hash Bucket: 3 Hash Value: 1323
Inserting Hash Bucket: 4 Hash Value: 6173
Inserting Hash Bucket: 9 Hash Value: 4199
Inserting Hash Bucket: 5 Hash Value: 4344
Inserting Hash Bucket: 0 Hash Value: 9679
Inserting Hash Bucket: 8 Hash Value: 1989
--- 5.1.c) QuadraticProbing Hash Table ---
Hash Bucket: 0 Hash Value: 9679
Hash Bucket: 1 Hash Value: 4371
Hash Bucket: 2 Hash Value: 0
Hash Bucket: 3 Hash Value: 1323
Hash Bucket: 4 Hash Value: 6173
Hash Bucket: 5 Hash Value: 4344
Hash Bucket: 6 Hash Value: 0
Hash Bucket: 7 Hash Value: 0
Hash Bucket: 8 Hash Value: 1989
Hash Bucket: 9 Hash Value: 4199
--- 5.1.b) Inserting Book Data into Double Hash Table ---
Inserting Hash Bucket: 1 Hash Value: 4371
Inserting Hash Bucket: 3 Hash Value: 1323
Inserting Hash Bucket: 4 Hash Value: 6173
Inserting Hash Bucket: 9 Hash Value: 4199
Inserting Hash Bucket: 7 Hash Value: 4344
Inserting Hash Bucket: 5 Hash Value: 9679
Could not insert 1989
--- 5.1.d) Double Hash Table ---
Hash Bucket: 0 Hash Value: 0
Hash Bucket: 1 Hash Value: 4371
Hash Bucket: 2 Hash Value: 0
Hash Bucket: 3 Hash Value: 1323
Hash Bucket: 4 Hash Value: 6173
Hash Bucket: 5 Hash Value: 9679
Hash Bucket: 6 Hash Value: 0
Hash Bucket: 7 Hash Value: 4344
Hash Bucket: 8 Hash Value: 0
Hash Bucket: 9 Hash Value: 4199

*/

#include <iostream>
#include "single_linked_list.h"

using namespace std;

#define BUCKETS 10
#define EMPTY_BUCKET -1

class SimpleHash{

	public:
		int input[BUCKETS];

		// Member Functions 
		void hashInsert(int value) {
			int hash_key = value % BUCKETS;
			input[hash_key] = value; 
			cout << "Inserting Hash Bucket: " << hash_key << " Hash Value: " << value << endl;
		}

		void hashRemove(int value) {
			int hash_key = value % BUCKETS;
			if(input[hash_key] == value){
				cout << "Removing Hash Bucket: " << hash_key << " Hash Value: " << value << endl;
				input[hash_key] = EMPTY_BUCKET;
			} else {
				cout << "Item not found in bucket." << endl;
			}
		}

		void hashSearch(int value) {
			int hash_key = value % BUCKETS;
			if(input[hash_key] == value){
				cout << "Found Hash Bucket: " << hash_key << " Hash Value: " << value << endl;
			} else {
				cout << "Item not found in bucket." << endl;
			}
		}

		void hashPrint() {
			for(int i = 0; i < sizeof(input)/sizeof(input[0]); i++) {
				cout << "Hash Bucket: " << i << " Hash Value: " << input[i] << endl;
			}
		}

};

class ChainingHash
{ 
    public: 
  		// Each bucket will be a Single Linked List
  		SingleList hash_table[BUCKETS];

		// Member Functions 
		void hashInsert(int value) {
			int hash_key = value % BUCKETS;
			hash_table[hash_key].append(value);
			cout << "Inserting Hash Bucket: " << hash_key << " Hash Value: " << value << endl;
		}

		void hashRemove(int value) {
			int hash_key = value % BUCKETS;
			
			/* malloc tmp node */
			struct Node *tmp = (struct Node*) malloc(sizeof(struct Node)); 
		
			/* set tmp node to head */
			tmp=hash_table[hash_key].head;

			// Check if head is value to be deleted
			if (tmp->data == value){
				hash_table[hash_key].head = tmp->next; 
			}
			else{
				while( tmp->next != NULL){
					/* malloc prev node */
					struct Node *prev = (struct Node*) malloc(sizeof(struct Node));
					
					prev = tmp;
					
					tmp = tmp->next;

					if(tmp->data == value){
						prev->next = tmp->next;
					}
				}
			}
		}

		void hashSearch(int value) {
			int hash_key = value % BUCKETS;
			int found = 0;
			
			/* malloc tmp node */
			struct Node *tmp = (struct Node*) malloc(sizeof(struct Node)); 
		
			/* set tmp node to head */
			tmp=hash_table[hash_key].head;

			while( tmp != NULL){
				if(tmp->data == value){
					found = 1;
					cout << "Found Hash Bucket: " << hash_key << " Hash Value: " << value << endl;
				}
				tmp = tmp->next;
			}

			if(!found) { cout << "Item not found in bucket." << endl; }
		}

		void hashPrint() {
			for(int i = 0; i < sizeof(hash_table)/sizeof(hash_table[0]); i++) {
				/* malloc tmp node */
				struct Node *tmp = (struct Node*) malloc(sizeof(struct Node)); 
			
				/* set tmp node to head */
				tmp=hash_table[i].head;
				cout << "Hash Bucket: " << i << " None";
				while( tmp != NULL){
					 cout << " -> " << tmp->data;
					tmp = tmp->next;
				}
				cout << "\n" ;
		}
	}

}; 

class LinearProbing{

	public:
		int input[BUCKETS];
		int empty_bucket_since[BUCKETS] = {1,1,1,1,1,1,1,1,1,1};

		// Member Functions 
		void hashInsert(int value) {
			/* Get hash key */
			int hash_key = value % BUCKETS;

			/* var to track if value inserted */
			int inserted = 0;

			/* If bucket is empty insert value */
			if(empty_bucket_since[hash_key]){
				input[hash_key] = value; empty_bucket_since[hash_key] = 0; inserted =1;
				cout << "Inserting Hash Bucket: " << hash_key << " Hash Value: " << value << endl;
			} else { /* Else bucket is populated */
				/* Get original hash bucket */
				int original_key = hash_key;

				/* Set new hsh bucket/key to value+1 % BUCKETS */
				int tmp_val = value;
				hash_key = (++tmp_val) % BUCKETS;

				/* Loop through buckets until original key is hit */
				while(hash_key != original_key){
					/* If bucket is empty insert value */
					if(empty_bucket_since[hash_key]){
						input[hash_key] = value; empty_bucket_since[hash_key] = 0; inserted =1;
						cout << "Inserting Hash Bucket: " << hash_key << " Hash Value: " << value << endl;

						/* leave loop since it's inserted */
						break;
					}
					/* Increment hash bucket */
					hash_key = (++tmp_val) % BUCKETS;
				}

				if(!inserted){ cout << "Could not insert "<< value << endl; }
			}

		}

		void hashRemove(int value) {
			for(int i = 0; i < sizeof(input)/sizeof(input[0]); i++) {
				if(input[i] == value){
					cout << "Removing Hash Bucket: " << i << " Hash Value: " << value << endl;
					input[i] = EMPTY_BUCKET;
				} else {
					cout << "Item not found in bucket." << endl;
				}
			}
		}

		void hashSearch(int value) {
			for(int i = 0; i < sizeof(input)/sizeof(input[0]); i++) {
				if(input[i] == value){
					cout << "Found Hash Bucket: " << i << " Hash Value: " << value << endl;
				} else {
					cout << "Item not found in bucket." << endl;
				}
			}
		}

		void hashPrint() {
			for(int i = 0; i < sizeof(input)/sizeof(input[0]); i++) {
				cout << "Hash Bucket: " << i << " Hash Value: " << input[i] << endl;
			}
		}

};

class QuadraticProbing{

	public:
		int input[BUCKETS];
		int empty_bucket_since[BUCKETS] = {1,1,1,1,1,1,1,1,1,1};

		// Member Functions 
		void hashInsert(int value) {
			/* var for quadratic */
			int iteration = 0;
			
			/* Get hash key */
			int hash_key = ((value % BUCKETS) + (iteration*iteration)) % BUCKETS;

			/* var to track if value inserted */
			int inserted = 0;

			/* If bucket is empty insert value */
			if(empty_bucket_since[hash_key]){
				input[hash_key] = value; empty_bucket_since[hash_key] = 0; inserted =1;
				cout << "Inserting Hash Bucket: " << hash_key << " Hash Value: " << value << endl;
			} else { /* Else bucket is populated */
				/* Get original hash bucket */
				int original_key = hash_key;

				/* Set new hsh bucket/key to iteration+1 % BUCKETS */
				++iteration;
				hash_key = ((value % BUCKETS) + (iteration*iteration)) % BUCKETS;

				/* Loop through buckets until original key is hit */
				while(hash_key != original_key){
					/* If bucket is empty insert value */
					if(empty_bucket_since[hash_key]){
						input[hash_key] = value; empty_bucket_since[hash_key] = 0; inserted =1;
						cout << "Inserting Hash Bucket: " << hash_key << " Hash Value: " << value << endl;

						/* leave loop since it's inserted */
						break;
					}
					/* Increment hash bucket */
					/* Set new hsh bucket/key to iteration+1 % BUCKETS */
					++iteration;
					hash_key = ((value % BUCKETS) + (iteration*iteration)) % BUCKETS;
				}

				if(!inserted){ cout << "Could not insert "<< value << endl; }
			}

		}

		void hashRemove(int value) {
			for(int i = 0; i < sizeof(input)/sizeof(input[0]); i++) {
				if(input[i] == value){
					cout << "Removing Hash Bucket: " << i << " Hash Value: " << value << endl;
					input[i] = EMPTY_BUCKET;
				} else {
					cout << "Item not found in bucket." << endl;
				}
			}
		}

		void hashSearch(int value) {
			for(int i = 0; i < sizeof(input)/sizeof(input[0]); i++) {
				if(input[i] == value){
					cout << "Found Hash Bucket: " << i << " Hash Value: " << value << endl;
				} else {
					cout << "Item not found in bucket." << endl;
				}
			}
		}

		void hashPrint() {
			for(int i = 0; i < sizeof(input)/sizeof(input[0]); i++) {
				cout << "Hash Bucket: " << i << " Hash Value: " << input[i] << endl;
			}
		}

};

class DoubleHash{

	public:
		int input[BUCKETS];
		int empty_bucket_since[BUCKETS] = {1,1,1,1,1,1,1,1,1,1};

		// Member Functions 
		void hashInsert(int value) {
			/* var for quadratic */
			int iteration = 0;
			
			/* Get hash key */
			int hash_key = value % BUCKETS;
			int hash_key_2 = 7 - (value % 7);

			/* var to track if value inserted */
			int inserted = 0;

			/* If bucket is empty insert value */
			if(empty_bucket_since[hash_key]){
				input[hash_key] = value; empty_bucket_since[hash_key] = 0; inserted =1;
				cout << "Inserting Hash Bucket: " << hash_key << " Hash Value: " << value << endl;
			} else { /* Else bucket is populated */
				/* Get original hash bucket */
				int original_key = hash_key;

				/* Set new hsh bucket/key to iteration+1 % BUCKETS */
				++iteration;
				hash_key = ((value % BUCKETS) + iteration*(hash_key_2)) % BUCKETS;

				/* Loop through buckets until original key is hit */
				while(hash_key != original_key){
					/* If bucket is empty insert value */
					if(empty_bucket_since[hash_key]){
						input[hash_key] = value; empty_bucket_since[hash_key] = 0; inserted =1;
						cout << "Inserting Hash Bucket: " << hash_key << " Hash Value: " << value << endl;

						/* leave loop since it's inserted */
						break;
					}
					/* Increment hash bucket */
					/* Set new hsh bucket/key to iteration+1 % BUCKETS */
					++iteration;
					hash_key = ((value % BUCKETS) + iteration*(hash_key_2)) % BUCKETS;
				}

				if(!inserted){ cout << "Could not insert "<< value << endl; }
			}

		}

		void hashRemove(int value) {
			for(int i = 0; i < sizeof(input)/sizeof(input[0]); i++) {
				if(input[i] == value){
					cout << "Removing Hash Bucket: " << i << " Hash Value: " << value << endl;
					input[i] = EMPTY_BUCKET;
				} else {
					cout << "Item not found in bucket." << endl;
				}
			}
		}

		void hashSearch(int value) {
			for(int i = 0; i < sizeof(input)/sizeof(input[0]); i++) {
				if(input[i] == value){
					cout << "Found Hash Bucket: " << i << " Hash Value: " << value << endl;
				} else {
					cout << "Item not found in bucket." << endl;
				}
			}
		}

		void hashPrint() {
			for(int i = 0; i < sizeof(input)/sizeof(input[0]); i++) {
				cout << "Hash Bucket: " << i << " Hash Value: " << input[i] << endl;
			}
		}

};

int main() 
{

	/* Init Simple Hash which does not account for collisions */
	SimpleHash *simple_hash = new SimpleHash();

	cout << "--- Inserting Dummy Data into Simple Has ---" << endl;

	/* Populate Simple Hash w/ List that has no collisions */
	int input_no_collision[BUCKETS] = { 1, 15, 12, 24, 33, 39, 40, 77, 56, 18 };
	for (int i = 0; i < sizeof(input_no_collision)/sizeof(input_no_collision[0]); ++i){
		simple_hash->hashInsert(input_no_collision[i]);
	}

	simple_hash->hashSearch(24);
	simple_hash->hashRemove(24);
	cout << "--- Simple Hash Table ---" << endl;
	simple_hash->hashPrint();

	/* Init Chain Hash which accounts for collisions using Chaining */
	ChainingHash *chain_hash = new ChainingHash();

	cout << "--- 5.1.a) Inserting Book Data into Chain Hash Table ---" << endl;

	/* Populate Chain Hash w/ List from book */
	int input_from_book[7] 	= { 4371, 1323, 6173, 4199, 4344, 9679, 1989};
	for (int i = 0; i < sizeof(input_from_book)/sizeof(input_from_book[0]); ++i){
		chain_hash->hashInsert(input_from_book[i]);
	}

	cout << "--- 5.1.a) Chaining Hash Table ---" << endl;
	chain_hash->hashPrint();

	/* Init Linear Probin Hash which accounts for collisions using Chaining */
	LinearProbing *linear_probing_hash = new LinearProbing();

	cout << "--- 5.1.b) Inserting Book Data into Linear Probing Hash Table ---" << endl;

	/* Populate Linear Hash w/ List from book */
	for (int i = 0; i < sizeof(input_from_book)/sizeof(input_from_book[0]); ++i){
		linear_probing_hash->hashInsert(input_from_book[i]);
	}

	cout << "--- 5.1.b) Linear Probing Hash Table ---" << endl;
	linear_probing_hash->hashPrint();

	/* Init Quadratic Probin Hash which accounts for collisions using Chaining */
	QuadraticProbing *quadratic_probing_hash = new QuadraticProbing();

	cout << "--- 5.1.b) Inserting Book Data into Quadratic Probing Hash Table ---" << endl;

	/* Populate Quad Hash w/ List from book */
	for (int i = 0; i < sizeof(input_from_book)/sizeof(input_from_book[0]); ++i){
		quadratic_probing_hash->hashInsert(input_from_book[i]);
	}
	cout << "--- 5.1.c) QuadraticProbing Hash Table ---" << endl;
	quadratic_probing_hash->hashPrint();

	/* Init Double Hash which accounts for collisions using Chaining */
	DoubleHash *double_hash = new DoubleHash();

	cout << "--- 5.1.b) Inserting Book Data into Double Hash Table ---" << endl;

	/* Populate Double Hash w/ List from book */
	for (int i = 0; i < sizeof(input_from_book)/sizeof(input_from_book[0]); ++i){
		double_hash->hashInsert(input_from_book[i]);
	}
	cout << "--- 5.1.d) Double Hash Table ---" << endl;
	double_hash->hashPrint();

    
    return 0;
}
