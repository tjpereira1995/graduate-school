import RPi.GPIO as GPIO
import time

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import logging
import time
import argparse
import json

# Allowed MQTT Actions
AllowedActions = ['both', 'publish', 'subscribe']

# MQTT Globals
host = "a1dmrfqnuqwe7l-ats.iot.us-east-1.amazonaws.com"
rootCAPath = "certs/root-CA.crt"
certificatePath = "certs/RPi-Motion-Sensor.cert.pem"
privateKeyPath = "certs/RPi-Motion-Sensor.private.key"
port = "8883"
useWebsocket = False
clientId = "motion_sensor"
topic = "motion_sensor"
mode = "publish"

def init_logging():
    # Configure logging
    logger = logging.getLogger("AWSIoTPythonSDK.core")
    logger.setLevel(logging.DEBUG)
    streamHandler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler.setFormatter(formatter)
    logger.addHandler(streamHandler)

def init_MQTT():
    # Init AWSIoTMQTTClient
    myAWSIoTMQTTClient = None
    myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
    myAWSIoTMQTTClient.configureEndpoint(host, port)
    myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

    # AWSIoTMQTTClient connection configuration
    myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
    myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)          # Infinite offline Publish queueing
    myAWSIoTMQTTClient.configureDrainingFrequency(2)                # Draining: 2 Hz
    myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)        # 10 sec
    myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)             # 5 sec

    # Connect to AWS IoT
    myAWSIoTMQTTClient.connect()
    #if mode == 'both' or mode == 'subscribe':
    #    myAWSIoTMQTTClient.subscribe(topic, 1, customCallback)

    return myAWSIoTMQTTClient

def publish_mqtt_msg(mqtt_client, reading, count):
        message = {}
        message['type']  = "motion"
        message['val']   = reading
        message['count'] = count
        message_json = json.dumps(message)

        mqtt_client.publish(topic, message_json, 1)

        if mode == 'publish':
            print('Published topic %s: %s\n' % (topic, message_json))

def main():
    # Main program block
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(20, GPIO.IN)
    GPIO.setup(21, GPIO.OUT)

    time.sleep(5)

    init_logging()

    client = init_MQTT()

    msg_count = 0
    while True:
        i = GPIO.input(20)

        if i == 0: # When output from motion sensor is LOW
            print "No motion",i
            GPIO.output(21, 0)
            time.sleep(0.5)

        elif i == 1: # When output from motion sensor is HIGH
            print "Motion detected",i
            GPIO.output(21, 1)
            time.sleep(5) # Cooldown

            publish_mqtt_msg(client, "1", msg_count)
            msg_count += 1

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
