import RPi.GPIO as GPIO #import the GPIO library
import time

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import logging
import time
import argparse
import json

# Allowed MQTT Actions
AllowedActions = ['both', 'publish', 'subscribe']

# MQTT Globals
host = "a1dmrfqnuqwe7l-ats.iot.us-east-1.amazonaws.com"
rootCAPath = "certs/root-CA.crt"
certificatePath = "certs/RPi-Reed-Sensor.cert.pem"
privateKeyPath = "certs/RPi-Reed-Sensor.private.key"
port = "8883"
useWebsocket = False
clientId = "reed_sensor"
topic = "reed_sensor"
mode = "publish"

def init_logging():
    # Configure logging
    logger = logging.getLogger("AWSIoTPythonSDK.core")
    logger.setLevel(logging.DEBUG)
    streamHandler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler.setFormatter(formatter)
    logger.addHandler(streamHandler)

def init_MQTT():
    # Init AWSIoTMQTTClient
    myAWSIoTMQTTClient = None
    myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
    myAWSIoTMQTTClient.configureEndpoint(host, port)
    myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

    # AWSIoTMQTTClient connection configuration
    myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
    myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)          # Infinite offline Publish queueing
    myAWSIoTMQTTClient.configureDrainingFrequency(2)                # Draining: 2 Hz
    myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)        # 10 sec
    myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)             # 5 sec

    # Connect to AWS IoT
    myAWSIoTMQTTClient.connect()

    return myAWSIoTMQTTClient

def publish_mqtt_msg(mqtt_client, reading, count):
        message = {}
        message['type']  = "reed"
        message['val']   = reading
        message['count'] = count
        message_json = json.dumps(message)

        mqtt_client.publish(topic, message_json, 1)

        if mode == 'publish':
            print('Published topic %s: %s\n' % (topic, message_json))


def main():
    # Main program block
    # Init GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(19, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    init_logging()

    client = init_MQTT()

    msg_count = 0

    while True:
        if GPIO.input(19):
            print("Door is open")

            publish_mqtt_msg(client, "1", msg_count)
            msg_count += 1

        if GPIO.input(19) == False:
            print("Door is closed")

        time.sleep(1)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
