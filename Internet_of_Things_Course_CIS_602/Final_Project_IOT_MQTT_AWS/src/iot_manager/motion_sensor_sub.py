#!/usr/bin/python

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import boto3
import logging
import time
import argparse
import json

# MQTT Globals
host = "a1dmrfqnuqwe7l-ats.iot.us-east-1.amazonaws.com"
rootCAPath = "certs/root-CA.crt"
certificatePath = "certs/IoT-Manager.cert.pem"
privateKeyPath = "certs/IoT-Manager.private.key"
port = "8883"
useWebsocket = False
clientId = "iot_manager_motion"
topic = "motion_sensor"

############################################################################
# sendSmsMsg - Send SMS alert to user
#   Params
#       msg - messgae to send to user
############################################################################
def sendSmsMsg(msg):
    # NOTE: These keys change everytime you login
    sns = boto3.client(
            "sns",
            aws_access_key_id="ASIARNQQRESV6EFACM7C",
            aws_secret_access_key="KRqnTHB+qCTXlgbAJjT6AVgWvQBadurRHBFeSoyj",
            aws_session_token="FwoGZXIvYXdzEL3//////////wEaDNo8JSBgIW+aw3gu2yLDARWiIb+ZRDwJiSLtPO+8UCkZyHWNHYubBFXj5ANT2i1Sh7ji+ZCu8x1x8ptnPWkT2BKbc9KlB96CGrJh4pS2l5kSsGvmckxKOWeBur99FLI5u52o95Uc4IICMAJ3dRDxFRwtPGYFiGEovLQUeTLD6Z0vw2y31EwuYb4vRYm8fsKGC+R1JhGqKquve6x9ITx4eq5Xn9w3VUENXjB9XfntNf6qrZENUPagWNRal70BVABz+ItGlEXINIOxtDuHtybwnVabcyixsYX+BTItzH6kpI5bCaXIOo6q3ej5u7S10x7HovOED/6iFCPamWXnhXxeFtdy9N39XR1A",
            region_name="us-east-1"
        )

        # Send a SMS message to the specified phone number
    response = sns.publish(
            PhoneNumber='+17746349806',
            Message=msg,
            MessageAttributes = {
                   'AWS.SNS.SMS.SMSType': {
                        'DataType': 'String',
                        'StringValue': 'Transactional'
                        }
                   }
        )
############################################################################
# END FUNCTION
############################################################################

############################################################################
# insertDynamoDbItem - Insert DyanmoDB Item
#   Params
#       value - item value (sensor reading)
#       event_type - item type to be written to DB
############################################################################
def insertDynamoDbItem(value, event_type):
        # Create boto session
	session = boto3.Session(
            aws_access_key_id="ASIARNQQRESV6EFACM7C",
            aws_secret_access_key="KRqnTHB+qCTXlgbAJjT6AVgWvQBadurRHBFeSoyj",
            aws_session_token="FwoGZXIvYXdzEL3//////////wEaDNo8JSBgIW+aw3gu2yLDARWiIb+ZRDwJiSLtPO+8UCkZyHWNHYubBFXj5ANT2i1Sh7ji+ZCu8x1x8ptnPWkT2BKbc9KlB96CGrJh4pS2l5kSsGvmckxKOWeBur99FLI5u52o95Uc4IICMAJ3dRDxFRwtPGYFiGEovLQUeTLD6Z0vw2y31EwuYb4vRYm8fsKGC+R1JhGqKquve6x9ITx4eq5Xn9w3VUENXjB9XfntNf6qrZENUPagWNRal70BVABz+ItGlEXINIOxtDuHtybwnVabcyixsYX+BTItzH6kpI5bCaXIOo6q3ej5u7S10x7HovOED/6iFCPamWXnhXxeFtdy9N39XR1A",
	)

        # Open dynamo handler
	dynamodb = session.resource('dynamodb', region_name='us-east-1')

        # Select table
	table = dynamodb.Table('IoT_Sensor_Event_Log')

        # Write item to table
	timestamp = int(time.time())
	response = table.put_item(
		Item={
			'Trigger_Event': event_type,
			'Epoch_Date': timestamp,
			'Value': value      
		}
	)
#############################################################################
# END FUNCTION
#############################################################################

#############################################################################
# customCallBack - Callback signal when MQTT subscibed message recieved
#   Params
#       client - MQTT client
#       userdata - Just set to default 1
#       message - Message topic subscribed to
#############################################################################
def customCallback(client, userdata, message):
    # Print out message info for testing and demonstration
    print("Received a new message: ")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print("--------------\n\n")

    # Extract value and see if out of range
    msg_dict = json.loads(message.payload)
    sensor_reading = int(msg_dict['val'])

    if (sensor_reading == 1):
        print("Send Alert and save in dynamoDB")
	sendSmsMsg('Motion Alert.\nMotion was detected.')

        insertDynamoDbItem(sensor_reading, msg_dict['type'])
############################################################################
# END FUNCTION
############################################################################

############################################################################
# main processing function
############################################################################
def main():

    # Configure logging
    logger = logging.getLogger("AWSIoTPythonSDK.core")
    logger.setLevel(logging.DEBUG)
    streamHandler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler.setFormatter(formatter)
    logger.addHandler(streamHandler)

    # Init AWSIoTMQTTClient
    myAWSIoTMQTTClient = None
    if useWebsocket:
        myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId, useWebsocket=True)
        myAWSIoTMQTTClient.configureEndpoint(host, port)
        myAWSIoTMQTTClient.configureCredentials(rootCAPath)
    else:
        myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
        myAWSIoTMQTTClient.configureEndpoint(host, port)
        myAWSIoTMQTTClient.configureCredentials(rootCAPath, 
                                                privateKeyPath, 
                                                certificatePath)

    # AWSIoTMQTTClient connection configuration
    myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
    myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)
    myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
    myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
    myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

    # Connect and subscribe to AWS IoT
    myAWSIoTMQTTClient.connect()
    myAWSIoTMQTTClient.subscribe(topic, 1, customCallback)
    time.sleep(2)

    # Loop forever
    while True:
        time.sleep(1)

if __name__=="__main__": 
    main() 
