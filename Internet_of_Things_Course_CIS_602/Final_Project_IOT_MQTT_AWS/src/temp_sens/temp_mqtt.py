import time
import dht11
import RPi.GPIO as GPIO

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import logging
import time
import argparse
import json

# Allowed MQTT Actions
AllowedActions = ['both', 'publish', 'subscribe']

# MQTT Globals
host = "a1dmrfqnuqwe7l-ats.iot.us-east-1.amazonaws.com"
rootCAPath = "certs/root-CA.crt"
certificatePath = "certs/RPi-Temp-Sensor.cert.pem"
privateKeyPath = "certs/RPi-Temp-Sensor.private.key"
port = "8883"
useWebsocket = False
clientId = "temp_sensor"
topic = "temp_sensor"
mode = "publish"

#define GPIO 14 as DHT11 data pin
Temp_sensor = 26

def init_logging():
    # Configure logging
    logger = logging.getLogger("AWSIoTPythonSDK.core")
    logger.setLevel(logging.DEBUG)
    streamHandler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler.setFormatter(formatter)
    logger.addHandler(streamHandler)

def init_MQTT():
    # Init AWSIoTMQTTClient
    myAWSIoTMQTTClient = None
    myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
    myAWSIoTMQTTClient.configureEndpoint(host, port)
    myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

    # AWSIoTMQTTClient connection configuration
    myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
    myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)          # Infinite offline Publish queueing
    myAWSIoTMQTTClient.configureDrainingFrequency(2)                # Draining: 2 Hz
    myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)        # 10 sec
    myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)             # 5 sec

    # Connect to AWS IoT
    myAWSIoTMQTTClient.connect()
    #if mode == 'both' or mode == 'subscribe':
    #    myAWSIoTMQTTClient.subscribe(topic, 1, customCallback)

    return myAWSIoTMQTTClient

def publish_mqtt_msg(mqtt_client, reading, count):
        message = {}
        message['type']  = "temp"
        message['val']   = reading
        message['count'] = count
        message_json = json.dumps(message)

        mqtt_client.publish(topic, message_json, 1)

        if mode == 'publish':
            print('Published topic %s: %s\n' % (topic, message_json))

def main():
    # Main program block
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM) # Use BCM GPIO numbers
    instance = dht11.DHT11(pin = Temp_sensor)

    init_logging()

    client = init_MQTT()

    msg_count = 0
    while True:
        #get DHT11 sensor value
        result = instance.read()
        if (result.temperature > 0):
            print "Temperature = ",result.temperature,"C"," Humidity = ",result.humidity,"%"
            publish_mqtt_msg(client, str(result.temperature), msg_count)
            msg_count +=1
        time.sleep(1)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
