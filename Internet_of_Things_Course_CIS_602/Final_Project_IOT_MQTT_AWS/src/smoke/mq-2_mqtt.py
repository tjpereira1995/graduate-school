import RPi.GPIO as GPIO
import time

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import logging
import time
import argparse
import json

# Allowed MQTT Actions
AllowedActions = ['both', 'publish', 'subscribe']

# MQTT Globals
host = "a1dmrfqnuqwe7l-ats.iot.us-east-1.amazonaws.com"
rootCAPath = "certs/root-CA.crt"
certificatePath = "certs/RPi-Smoke-Sensor.cert.pem"
privateKeyPath = "certs/RPi-Smoke-Sensor.private.key"
port = "8883"
useWebsocket = False
clientId = "smoke_sensor"
topic = "smoke_sensor"
mode = "publish"

# SPI port on the ADC to the Cobbler
SPICLK = 11
SPIMISO = 9
SPIMOSI = 10
SPICS = 8
mq2_dpin = 26
mq2_apin = 0

def init_logging():
    # Configure logging
    logger = logging.getLogger("AWSIoTPythonSDK.core")
    logger.setLevel(logging.DEBUG)
    streamHandler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler.setFormatter(formatter)
    logger.addHandler(streamHandler)

def init_MQTT():
    # Init AWSIoTMQTTClient
    myAWSIoTMQTTClient = None
    myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
    myAWSIoTMQTTClient.configureEndpoint(host, port)
    myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

    # AWSIoTMQTTClient connection configuration
    myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
    myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)          # Infinite offline Publish queueing
    myAWSIoTMQTTClient.configureDrainingFrequency(2)                # Draining: 2 Hz
    myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)        # 10 sec
    myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)             # 5 sec

    # Connect to AWS IoT
    myAWSIoTMQTTClient.connect()

    return myAWSIoTMQTTClient

def publish_mqtt_msg(mqtt_client, reading, count):
    message = {}
    message['type']  = "smoke"
    message['val']   = reading
    message['count'] = count
    message_json = json.dumps(message)

    mqtt_client.publish(topic, message_json, 1)

    if mode == 'publish':
        print('Published topic %s: %s\n' % (topic, message_json))


# Init GPIO
def init():
    GPIO.setwarnings(False)
    GPIO.cleanup()			# clean up at the end of your script
    GPIO.setmode(GPIO.BCM)	# to specify whilch pin numbering system

    # set up the SPI interface pins
    GPIO.setup(SPIMOSI, GPIO.OUT)
    GPIO.setup(SPIMISO, GPIO.IN)
    GPIO.setup(SPICLK, GPIO.OUT)
    GPIO.setup(SPICS, GPIO.OUT)
    GPIO.setup(mq2_dpin,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)

#read SPI data from MCP3008(or MCP3204) chip,8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
        GPIO.output(cspin, True)	

        GPIO.output(clockpin, False)  # start clock low
        GPIO.output(cspin, False)     # bring CS low

        commandout = adcnum
        commandout |= 0x18  # start bit + single-ended bit
        commandout <<= 3    # we only need to send 5 bits here
        for i in range(5):
                if (commandout & 0x80):
                        GPIO.output(mosipin, True)
                else:
                        GPIO.output(mosipin, False)
                commandout <<= 1
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)

        adcout = 0
        # read in one empty bit, one null bit and 10 ADC bits
        for i in range(12):
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)
                adcout <<= 1
                if (GPIO.input(misopin)):
                        adcout |= 0x1

        GPIO.output(cspin, True)
        
        adcout >>= 1       # first bit is 'null' so drop it
        return adcout


def main():
    init()
    time.sleep(10)
    init_logging()
    client = init_MQTT()

    msg_count = 0
    while True:
        COlevel=readadc(mq2_apin, SPICLK, SPIMOSI, SPIMISO, SPICS)
        if (COlevel <  1000):
            if COlevel < 70:
                print("No Smoke")
                time.sleep(.5)
            else:
                print("Smoke or Flame: FIRE!!!")
                time.sleep(.5)

            publish_mqtt_msg(client, COlevel, msg_count)
            msg_count += 1
        time.sleep(1)


if __name__ =='__main__':
    try:
        main()
        pass
    except KeyboardInterrupt:
        pass

GPIO.cleanup()
