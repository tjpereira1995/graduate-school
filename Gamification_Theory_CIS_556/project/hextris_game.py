import pygame
import random

import boto3
import operator


# Steps to making this game
# 1. Add name entry after you lose

"""
[default]
aws_access_key_id=ASIARNQQRESV2LRNC2OL
aws_secret_access_key=GHCiUrk2mxiE/GZlx29soyjkgMRjtXLAJvSVsyV1
aws_session_token=IQoJb3JpZ2luX2VjEBkaCXVzLXdlc3QtMiJHMEUCIQD1JxYMYDu87Hvne5JQwNdmusJMVdTrLDvayYtbrGCzIwIgQZr4O6mmEh+URfDuUVWDBfol12Si9yZGS4nuAghQn5wqtQIIgv//////////ARAAGgwwOTc3NDUxODM5MTUiDKUK9zLTlRrtM7l5ZyqJAkHE7REx80dlR8OqZiJizF/3h8+D75zeMzVmUq6a4DNTTpAKOLi3ZR3slQzmY4PR2UJYJzbcZNeTh3fBIpusLLKV/qkZW/e5WvCOFwiSUyyXidzdHtPpqwA7F0q+ugYSjRpPSaEmaHK1xL7gtDOkhxPKrQXx8mBLNnBtymBCI9cPPh1XjbL0D9HmNMD3DQKjMlTLIQMdqYr/QqLSC1quACyw3QZ2DSj96AX1GD+iPEBBrproHlCpCwZEYOxVLivuhwk2mqlPfn2VLaT5Ie4aPHseTvKpCzRoDMj2xG05w71cHAQ0m3/CC4lfKKIKXPMAd4Dm2qUsaKNZp60oEdg9MgU6Pd3wFWeIBWsw6sH4gwY6nQGtOoVjNiTx9A8n7+aj3i/lKZbVEh9g514VZAJwbujcM3HYisRslxE5gXZupdZEDajhw6J7iiu06MMClAcLBWU4orUt6X6QNx002v6CZqdEErzYsUY01oWSkxUpao77g450pxxYhEx+bXk+tbBK3la7rjTZVjfb4w5HAAXm6irsT9JRu1b4zgt9XFX68PUB6mwJKsJ96dTnD9v8Zhfs
"""

AWS_ACCESS_KEY_ID = 'ASIARNQQRESVXBQ35LGA'
AWS_SECRET_ACCESS_KEY = 'W6MR+1hW8xeCEjM3CNjtCxGoMwmFjunXJXL61YvI'
AWS_SESSION_TOKEN = 'IQoJb3JpZ2luX2VjEEgaCXVzLXdlc3QtMiJHMEUCIQCJvq/UzFzHNge9iei2vb1oPzlzsESNWcRD07ftBEjoqQIgWoI2rBCDgLCmcjDQbim4sQVo7OQhS8qiWVaKtQFoED8qtQIIsf//////////ARAAGgwwOTc3NDUxODM5MTUiDL2ZyS6vYYPvhAEyzSqJAq3an+u15f1o2R/8qhsrW/RxsoEZhbil+U5mVF9qQAr4E1chCKkU3Yx/yUfjytt+c2eJbxlF+oiIwvIgWq9pVIao0PSXvVFo6cw92hWqHvc69NOwO++F6vIyu9HA7Bq89/8Mv5c3WcYJIzLDDERYrFXoXQjKj6hHBeAHnNuZ7qcY3dy/CarMjSarqTT2jWb3MchEaORZQ0F1VTZdjLhPOU8hae+SZ8Cx4K9LJfj9v0vDgm5BBxiPbPwh2WZ6OSZxY8A+MJxWFANQYaTwU/ixVFtLtbQ5rpZbdL009+2RCXxTAO9wk+1ADWvu4ZEZ2o4yb0YTT/7AT9x26sNP+QnsWwGj3SehfsZ2D48w9u6ChAY6nQHvP2qmy3K1GaBin6zbEfRuYjSm5omrW2Ur9BD/Ha2Wq+XPqt4irIlMnUTwpLuijJUY0lfIXwh1OE8L1DMea0EC4dOljak5wTR9bNoecHGZQfYWK9anUlE9NxFsQ5yCTWkLQjJFXXUErI6/E87wHhD0ZhFsGjSLUl4Nz33oHV6qMbY3MTINAewKN+yFQERgsJ/j8wDKy8IzNxNHHPB7'


# Global variables
colors = [
    (0, 0, 0),
    (100, 179, 179),
    (120, 37, 179)
]

numbers = ['-1', '0', '1',]

# Globals used to try to balance 0s and 1s 50/50
total_zeroes = 0
total_drops = 0

# Screen size
screen_width = 500
screen_height = 700
size = (screen_width, screen_height)

# FPS
fps = 25
clock = pygame.time.Clock()

# RGB Colors
white  = (255, 255, 255)
black  = (0, 0, 0)
gray   = (128, 128, 128)
red    = (255, 0, 0)
green  = (0, 255, 0)
blue   = (0, 0, 255)
yellow =(255, 255, 0)

retro_font = "astron.ttf"

# This is a hextris block
class Figure:
    x = 0
    y = 0

    block = [1, 1, 1, 1]

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.color  = self.gen_number()
        self.number = self.color

    def image(self):
        return self.block

    def gen_number(self):
        global total_zeroes
        global total_drops

        num = random.randint(1, len(colors) - 1)

        # Add to num zeroes
        if num == 1:
            total_zeroes += 1

        # Add to total drops
        total_drops += 1

        # Find out percentage of 0s
        # Adjust number to keep it 50/50 of 0s and 1s
        if (((float(total_zeroes / total_drops) * 100.0) < 48.0) and (num == 2)):
            num = 1
            total_zeroes += 1

        elif (((float(total_zeroes / total_drops) * 100.0) > 52.0) and (num == 1)):
            num = 2
            total_zeroes -= 1

        return num


class Hextris:
    score = 0
    state = "start"
    field = []
    value = []
    height = 0
    width = 0
    x = 175
    y = 150
    zoom = 40
    figure = None

    def __init__(self, height, width, goal, level):
        self.height = height
        self.width = width
        self.field = []
        self.value = []
        self.level = level
        self.goal  = goal
        self.state = "start"
        self.pause = False

        for i in range(height):
            new_line = []
            for j in range(width):
                new_line.append(0)
            self.field.append(new_line)
            self.value.append(new_line)

    # Function to create a new block
    def new_figure(self):
        if self.level == 1:
            self.figure = Figure(0, 0)
        if self.level == 2:
            self.figure = Figure(1, 0)
        if self.level == 3:
            self.figure = Figure(1, 0)

    # Get hex number from hex blocks
    def get_hex_from_bin_arr(self, bin_arr):
        val = 0
        for i in range(0, len(bin_arr)):
            val |= bin_arr[i] << ((len(bin_arr) - 1) - i)

        return val

    def intersects(self):
        intersection = False
        for i in range(4):
            for j in range(4):
                if i * 4 + j in self.figure.image():
                    if i + self.figure.y > self.height - 1 or \
                            j + self.figure.x > self.width - 1 or \
                            j + self.figure.x < 0 or \
                            self.field[i + self.figure.y][j + self.figure.x] > 0:
                        intersection = True
        return intersection

    def break_lines(self):
        lines = 0
        for i in range(1, self.height):
            zeros = 0
            for j in range(self.width):
                if self.field[i][j] == 0:
                    zeros += 1
            # Full line
            if zeros == 0:
                # Check to see if we have matching hex number
                bin_arr = self.value[i]
                bin_arr = [x - 1 for x in bin_arr]
                #print(bin_arr)
                hex_val = self.get_hex_from_bin_arr(bin_arr)
                if hex_val == self.goal:
                    lines += 1
                    self.score += 1
                    if self.level == 1:
                        self.goal = random.randint(0, 3)
                    if self.level == 2:
                        self.goal = random.randint(0, 7)
                    if self.level == 3:
                        self.goal = random.randint(0, 15)
                    #print(self.goal)
                    for i1 in range(i, 1, -1):
                        for j in range(self.width):
                            self.field[i1][j] = self.field[i1 - 1][j]

    # Function to drop block to bottom
    def go_space(self):
        while not self.intersects():
            self.figure.y += 1
        self.figure.y -= 1
        self.freeze()

    # Function to drop block down one spot
    def go_down(self):
        self.figure.y += 1
        if self.intersects():
            self.figure.y -= 1
            self.freeze()

    # Function called at end of frame
    def freeze(self):
        for i in range(4):
            for j in range(4):
                if i * 4 + j in self.figure.image():
                    self.field[i + self.figure.y][j + self.figure.x] = self.figure.color
                    self.value[i + self.figure.y][j + self.figure.x] = self.figure.number

        self.break_lines()
        self.new_figure()
        if self.intersects():
            self.state = "gameover"

    def go_side(self, dx):
        old_x = self.figure.x
        self.figure.x += dx
        if self.intersects():
            self.figure.x = old_x


# Text Renderer
def text_format(message, textFont, textSize, textColor):
    newFont=pygame.font.Font(textFont, textSize)
    newText=newFont.render(message, 0, textColor)

    return newText


def game_over_menu(score):
    screen = pygame.display.set_mode(size)

    font = "astron.ttf"
    pos = 0

    name_arr = ["A", "A", "A"]

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                loop = 0
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key==pygame.K_UP:
                    name_arr[pos] = (chr(ord(name_arr[pos])+1))

                elif event.key==pygame.K_DOWN:
                    name_arr[pos] = (chr(ord(name_arr[pos])-1))

                if event.key==pygame.K_RETURN:
                    if pos < 2:
                        pos += 1
                    else:
                        put_score("".join(name_arr), int(score))
                        main_menu()


        title=text_format("Hextris", font, 48, yellow)

        text_name = text_format("".join(name_arr), font, 32, white)

        title_rect = title.get_rect()
        name_rect = text_name.get_rect()

        # Main Menu Text
        screen.blit(title, (screen_width/2 - (title_rect[2]/2), 80))
        screen.blit(text_name, (screen_width/2 - (name_rect[2]/2), 150))

        pygame.display.update()
        screen.fill((128, 128, 128))
        clock.tick(fps / 2)

def pause_menu(goal):
    loop = 1
    screen = pygame.display.set_mode(size)

    font = "astron.ttf"
    selected = ["resume", "hint", "quit"]
    key_pos = 0

    hint_selected = False

    while loop:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                loop = 0
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key==pygame.K_UP:
                    if key_pos != 0:
                        key_pos -= 1

                elif event.key==pygame.K_DOWN:
                    if key_pos != 2:
                        key_pos += 1

                if event.key==pygame.K_RETURN:
                    if selected[key_pos] == "resume":
                        loop = 0
                    if selected[key_pos] == "hint":
                        hint_selected = True

                    if selected[key_pos] == "quit":
                        loop = 0
                        pygame.quit()
                        quit()

                if event.key == pygame.K_ESCAPE:
                    loop = 0


        title=text_format("Hextris", font, 48, yellow)

        if selected[key_pos] == "resume":
            text_resume = text_format("RESUME", font, 32, white)
        else:
            text_resume = text_format("RESUME", font, 32, black)

        if selected[key_pos] == "quit":
            text_quit = text_format("QUIT", font, 32, white)
        else:
            text_quit = text_format("QUIT", font, 32, black)

        if selected[key_pos] == "hint":
            text_hint = text_format("HINT", font, 32, white)
        else:
            text_hint = text_format("HINT", font, 32, black)

        title_rect = title.get_rect()
        resume_rect = text_resume.get_rect()
        hint_rect = text_hint.get_rect()
        quit_rect = text_quit.get_rect()

        # Main Menu Text
        screen.blit(title, (screen_width/2 - (title_rect[2]/2), 80))
        screen.blit(text_resume, (screen_width/2 - (resume_rect[2]/2), 150))
        screen.blit(text_hint, (screen_width/2 - (hint_rect[2]/2), 200))
        screen.blit(text_quit, (screen_width/2 - (quit_rect[2]/2), 250))

        if (hint_selected):
            screen.blit(text_format("HINT: " + '{:04b}'.format(goal), font, 32, white), (20, 300))

        pygame.display.update()
        screen.fill((128, 128, 128))
        clock.tick(fps / 2)

def run_game():
    # Initialize the game engine
    screen = pygame.display.set_mode(size)

    pygame.display.set_caption("Hextris")

    bg = pygame.image.load("retro-1.png")

    # Loop until the user clicks the close button.
    done = False
    game = Hextris(12, 2, 0x0, 1)
    counter = 0
    total_score = 0

    pressing_down = False

    font_num = pygame.font.SysFont('Calibri', 16, True, False)

    while not done:
        if game.figure is None:
            game.new_figure()
        counter += 1
        if counter > 100000:
            counter = 0

        if counter % (fps // game.level // 2) == 0 or pressing_down:
            if game.state == "start":
                game.go_down()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_DOWN:
                    pressing_down = True
                if event.key == pygame.K_LEFT:
                    game.go_side(-1)
                if event.key == pygame.K_RIGHT:
                    game.go_side(1)
                if event.key == pygame.K_SPACE:
                    game.go_space()
                if event.key == pygame.K_ESCAPE:
                    game.pause = not game.pause
                    pause_menu(game.goal)


        if event.type == pygame.KEYUP:
                if event.key == pygame.K_DOWN:
                    pressing_down = False

        screen.blit(bg, (0, 0))
        
        # Draw board and existing blocks
        for i in range(game.height):
            for j in range(game.width):
                pygame.draw.rect(screen, white, [game.x + game.zoom * j, game.y + game.zoom * i, game.zoom, game.zoom], 1)
                if game.field[i][j] > 0:
                    pygame.draw.rect(screen, colors[game.field[i][j]],
                                     [game.x + game.zoom * j + 1, game.y + game.zoom * i + 1, game.zoom - 2, game.zoom - 1])

                    retro_num = pygame.font.Font(retro_font, 24)

                    screen.blit(retro_num.render(numbers[game.field[i][j]], True, white), 
                            (game.x + game.zoom * j + 12, game.y + game.zoom * i + 12))

        
        # Redraw the block for each frame
        if game.figure is not None:
            for i in range(game.height):
                for j in range(game.width):
                    p = i * 4 + j
                    if p in game.figure.image():
                        pygame.draw.rect(screen, colors[game.figure.color],
                                         [game.x + game.zoom * (j + game.figure.x) + 1,
                                          game.y + game.zoom * (i + game.figure.y) + 1,
                                          game.zoom - 2, game.zoom - 2])

                        retro_num = pygame.font.Font(retro_font, 24)

                        screen.blit(retro_num.render(numbers[game.figure.number], True, white), 
                            (game.x + game.zoom * (j + game.figure.x) + 12, game.y + game.zoom * (i + game.figure.y) + 12))


        font = pygame.font.SysFont('Calibri', 20, True, False)

        text = text_format("Score: " + str(total_score), retro_font, 20, black)

        cr_text = text_format("Create: " + str(hex(game.goal).upper()), retro_font, 20, black)

        level_text = text_format("Level: " + str(game.level), retro_font, 20, black)



        pygame.draw.rect(screen, gray, pygame.Rect(0, 0, screen_width, 50))

        screen.blit(text, [20, 0])
        screen.blit(cr_text, [screen_width - 125, 0])
        screen.blit(level_text, [20, 25])

        if (game.level == 1):
            total_score = game.score
        if (game.level == 2):
            total_score = game.score + 2
        if (game.level ==3):
            total_score = game.score + 4

        if total_score > 1 and game.level == 1:
            game = Hextris(12, 3, 0x0, 2)

        if total_score > 3 and game.level == 2:
            game = Hextris(12, 4, 0x0, 3)


        if game.state == "gameover":
            game_over_menu(total_score)

        pygame.display.flip()
        clock.tick(fps / 2)

    pygame.quit()


def put_score(name, score):
    # connect to AWS account
    client = boto3.client('dynamodb', aws_access_key_id = AWS_ACCESS_KEY_ID, 
                aws_secret_access_key = AWS_SECRET_ACCESS_KEY, region_name = 'us-east-1')

    session = boto3.Session(
        aws_access_key_id = AWS_ACCESS_KEY_ID,
        aws_secret_access_key = AWS_SECRET_ACCESS_KEY,
        aws_session_token = AWS_SESSION_TOKEN
    )

    dynamodb = session.resource('dynamodb', region_name='us-east-1')

    table = dynamodb.Table('Hextris_Scores_Table')
    response = table.put_item(
       Item={
                'name': str(name),
                'score': int(score)
            }
    )

def scan_db():
    # connect to AWS account
    client = boto3.client('dynamodb', aws_access_key_id = AWS_ACCESS_KEY_ID, 
                aws_secret_access_key = AWS_SECRET_ACCESS_KEY, region_name = 'us-east-1')

    session = boto3.Session(
        aws_access_key_id = AWS_ACCESS_KEY_ID,
        aws_secret_access_key = AWS_SECRET_ACCESS_KEY,
        aws_session_token = AWS_SESSION_TOKEN
    )

    dynamodb = session.resource('dynamodb', region_name='us-east-1')

    table = dynamodb.Table('Hextris_Scores_Table')
    response = table.scan()

    scores = {}
    items = response['Items']

    for item in items:
        scores[item['name']] = item['score']

    scores = dict(sorted(scores.items(), key=lambda item: item[1]))

    return scores

def leaderboard():
    screen = pygame.display.set_mode(size)

    font = "astron.ttf"
    selected = ["start", "leaderboard", "quit"]

    scores_db = scan_db()

    while True:
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                quit()
                pygame.quit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    main_menu()

        screen.fill(gray)
        title = text_format("Hextris", font, 48, yellow)
        text_scores = text_format("Top Scores", font, 32, white)

        title_rect = title.get_rect()
        scores_rect = text_scores.get_rect()


        # Main Menu Text
        screen.blit(title, (screen_width/2 - (title_rect[2]/2), 80))
        screen.blit(text_scores, (screen_width/2 - (scores_rect[2]/2), 120))

        i = 1
        for name, score in reversed(scores_db.items()):
            tmp_str = str(i) + ". " + str(name) + "  -  " + str(score)
            text_tmp = text_format(tmp_str , font, 24, white)

            rect_tmp = text_tmp.get_rect()
            screen.blit(text_tmp, (screen_width/2 - (rect_tmp[2]/2), (140 + 25 * (i+1)) ))
            i+=1

            if i == 20:
                break

        pygame.display.update()
        clock.tick(fps/2)
        pygame.display.set_caption("Hextris")

# Main Menu
def main_menu():

    pygame.init()

    screen = pygame.display.set_mode(size)

    menu = True

    font = "astron.ttf"
    selected = ["start", "leaderboard", "quit"]

    key_pos = 0

    while menu:
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                pygame.quit()
                quit()
            if event.type==pygame.KEYDOWN:
                if event.key==pygame.K_UP:
                    if key_pos != 0:
                        key_pos -= 1

                elif event.key==pygame.K_DOWN:
                    if key_pos != 2:
                        key_pos += 1

                if event.key==pygame.K_RETURN:
                    if selected[key_pos] == "start":
                        run_game()
                    if selected[key_pos] == "leaderboard":
                        leaderboard()
                    if selected[key_pos] == "quit":
                        pygame.quit()
                        quit()

        # Main Menu UI
        screen.fill(gray)
        title=text_format("Hextris", font, 48, yellow)

        if selected[key_pos] == "start":
            text_start = text_format("START", font, 32, white)
        else:
            text_start = text_format("START", font, 32, black)

        if selected[key_pos] == "quit":
            text_quit = text_format("QUIT", font, 32, white)
        else:
            text_quit = text_format("QUIT", font, 32, black)

        if selected[key_pos] == "leaderboard":
            text_board = text_format("LEADERBOARD", font, 32, white)
        else:
            text_board = text_format("LEADERBOARD", font, 32, black)

        title_rect = title.get_rect()
        start_rect = text_start.get_rect()
        board_rect = text_board.get_rect()
        quit_rect = text_quit.get_rect()

        # Main Menu Text
        screen.blit(title, (screen_width/2 - (title_rect[2]/2), 80))
        screen.blit(text_start, (screen_width/2 - (start_rect[2]/2), 150))
        screen.blit(text_board, (screen_width/2 - (board_rect[2]/2), 200))
        screen.blit(text_quit, (screen_width/2 - (quit_rect[2]/2), 250))
        pygame.display.update()
        clock.tick(fps/2)
        pygame.display.set_caption("Hextris")

#Initialize the Game
main_menu()