#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <errno.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <string.h> 
#include <time.h>

#define PORT 8080 /* alternative to port 80 which is commonly used */

/* Tyler Pereira
** 3/29/2019
** CIS 570
** References
** 		- https://www.gnu.org/software/libc/manual/html_node/Host-Names.html
** 		- https://www.geeksforgeeks.org/socket-programming-cc/
** 		- http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html
** 		- https://www.codingame.com/playgrounds/14213/how-to-play-with-strings-in-c/string-split
*/
int main(int argc, char const *argv[]) 
{ 
		/* first arg is file to transfer */
	    char *filename = argv[1];
	    
	    /* Get first item before split*/
	    char *token = strtok(argv[2], "@"); 

	    /* set dest file to token */
	    char *dest_filename = token;
	    
	    /* get computer name */
	    char *comp_name; 
	    while (token != NULL) { 
	        comp_name  = token;
	        token = strtok(NULL, "@"); 
	    }

	    char buffer[1024] = {0}; /* data buffer to recv responses */

	    /* Get host info */
	    char sender_host_name[256]; struct hostent *sender_host_info; 
	    char *sender_ip_addr; 

	    /* Get Host Name */ 
	    gethostname(sender_host_name, sizeof(sender_host_name)); 
	  
	    /* get Host Info */
	    sender_host_info = gethostbyname(sender_host_name); 
	  
	    /* Get IP Address of both sender and reciever since simulating on single machine*/
	    /* IF two seprate machines in use could set static IP on receiving machine or set specific host name */
	    /* To connect to receiver socket */
	    sender_ip_addr = inet_ntoa(*((struct in_addr*) sender_host_info->h_addr_list[0])); 

		int sock_result = 0;
		int recv_response; 

		struct sockaddr_in sender_addr; 

		/* create socket */
		sock_result = socket(AF_INET, SOCK_STREAM, 0);

		memset(&sender_addr, '0', sizeof(sender_addr)); 

		/* set socket IP addr and port */
		sender_addr.sin_family = AF_INET;
		inet_pton(AF_INET, sender_ip_addr, &sender_addr.sin_addr);
		sender_addr.sin_port = htons(PORT); 
		
		/* connect sender to port */
		connect(sock_result, (struct sockaddr *)&sender_addr, sizeof(sender_addr));

		/* Open file for reading */
		FILE *fp;
		fp = fopen(filename, "r");
		
		/* Get size of file */
		fseek(fp, 0, SEEK_END); // seek to end of file
		double size = ((double)ftell(fp) / 125000.0); // get current file pointer
		fseek(fp, 0, SEEK_SET); // seek back to beginning of file

		/* Init file sending */
		char * line = NULL;
    	size_t len = 0;

    	/* Send dest filename */
    	send(sock_result , dest_filename , strlen(dest_filename) , 0 ); 
    	recv_response = read( sock_result , buffer, 1024); 
		
		clock_t begin = clock(); /* used to calculate transfer time */
		
		/* Begin sending file until end of file reached*/
		while ((getline(&line, &len, fp)) != -1){
			strtok(line, "\n"); /* remove newline */

			/* Sned line to socket and receive response */
			send(sock_result , line , strlen(line) , 0 ); 
			recv_response = read( sock_result , buffer, 1024);

		}

		/* calculate latency and throughput */
		clock_t end = clock();
		double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		double throughput = size / time_spent;

		printf("File Transferred\n");
		printf("Size of File %lf Mb\n", size);
		printf("File transfer took %lf\n", time_spent);
		printf("Average Throughput was %lf (Mb/s)\n", throughput);

	return 0; 
} 
