#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <errno.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <string.h> 

#define PORT 8080 /* alternative to port 80 which is commonly used */

/* Tyler Pereira
** 3/29/2019
** CIS 570
** References
** 		- https://www.gnu.org/software/libc/manual/html_node/Host-Names.html
** 		- https://www.geeksforgeeks.org/socket-programming-cc/
** 		- http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html
*/

int main() 
{ 

	char host_name[256]; struct hostent *host_info; 
    char *ip_addr; 

    /* Get Host Name */ 
    gethostname(host_name, sizeof(host_name)); 
  
    /* get Host Info */
    host_info = gethostbyname(host_name); 
  
    /* Get IP receiver_addr of both sender and reciever since simulating on single machine*/
    /* IF two seprate machines in use could set static IP on receiving machine or set specific host name */
    /* To connect to receiver socket */
    ip_addr = inet_ntoa(*((struct in_addr*) host_info->h_addr_list[0]));


	int receiver_file_descriptor;			/* status of socket file descriptor */
	int socket_sts;							/* status of socket connection */
	int read_sts; 							/* status of reading from socket */

	struct sockaddr_in receiver_addr; 		/* socket adddr */
	int addrlen = sizeof(receiver_addr); 	/* length fo socket addr */

	char buffer[1024] = {0}; 				/* socket data buffer */
	char *response = "Received line"; 		/* response to send after receiving */
	
	/* create socket */
	receiver_file_descriptor = socket(AF_INET, SOCK_STREAM, 0);
	
	/* set socket IP addr and port */
	receiver_addr.sin_family = AF_INET; 
	inet_pton(AF_INET, ip_addr, &receiver_addr.sin_addr); 
	receiver_addr.sin_port = htons( PORT ); 
	
	/* bind socker to port 8080 */
	bind(receiver_file_descriptor, (struct sockaddr *)&receiver_addr, sizeof(receiver_addr));

	/* begin listening on socket */
	listen(receiver_file_descriptor, 5);

	/* loop so that socket never closes and receiver listens forever */
	while((socket_sts = accept(receiver_file_descriptor, (struct sockaddr *)&receiver_addr, (socklen_t*)&addrlen))>0){
		
		/* First thing sent from sender is new filename */
		char filename[32] = {0};

		/* Read filename from socket and send response back to sender */
		read_sts = read( socket_sts , filename, 32); 
		send(socket_sts , response , strlen(response) , 0 ); 

		/* Open file descript and clear if it exists already */
		FILE * fp;
		fp = fopen(filename, "w");fclose(fp);

		/* Continue to read from socket until sender finishes sending file */
		while(1){
			/* reset the buffer */
			memset(buffer, 0, sizeof(buffer));

			/* Read from socket until sender is done sending*/
			if ((read_sts = read( socket_sts , buffer, 1024)) > 0){
				
				/* Open and append line to file */
				fp = fopen(filename, "a");
				fprintf(fp, "%s\n", buffer);
				fclose(fp);

				/* Send Response to sender */
				send(socket_sts , response , strlen(response) , 0 ); 
			} else { break; }

		}
	}

	return 0; 
} 