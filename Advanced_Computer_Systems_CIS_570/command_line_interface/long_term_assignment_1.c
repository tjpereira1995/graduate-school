/* Tyler Pereira 	*/
/* 2/18/2019		*/
/* CIS-570			*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>

#include <sys/types.h>
#include <sys/wait.h>

#define MAX_PATH_CHAR_CNT 	256
#define MAX_ARGS 			32

/* GLOBALS */
int background_process = 0;
int is_piped = 0;
char *redirect_file[MAX_ARGS];
char *argv_pipe[MAX_ARGS];

void parse_cmd(char *cmd, char *unix_cmd, char **argv){

	int argv_index = 1;
	int argv_pipe_index = 0;
	int argv_complete = 0;
	char *tmp;

	/* Set first arg to unix cmd 
	** And continue parsing other args
	*/
	tmp = strtok(cmd, " "); 

	/* If user enters unix command just use first token as argv[0]*/
	if (strcmp(unix_cmd, "GENERIC_CMD") == 0){  
		argv[0] = tmp;
	} else { /* Else use passed command since dos command was entered */
		argv[0] = unix_cmd;
	}
	
	/* Move to next token in input */
	tmp = strtok (NULL, " ");

	while (tmp != NULL) {	
		if ((strcmp(tmp, ">") == 0 ) || (strcmp(tmp, "<") == 0 )) {
			/* Parse file redirect */
			/* redirect_file[0] holds direction */
			redirect_file[0] = tmp;
			tmp = strtok (NULL, " ");

			/* redirect[1] holds filename */
			redirect_file[1] = tmp;

			argv_complete = 1;
		} 

		/* Check for Ampersand at end of command */
		/* If ampersand found run cmd as background process */
		if (strcmp(tmp, "&") == 0 ){
			/* Set global background_process var */
			background_process = 1;
			argv_complete = 1;
			
		}

		if (strcmp(tmp, "|") == 0 ){
			/* set global piping var */
			is_piped = 1;

			/* Move to next token so | is not included in argv_pipe */
			tmp = strtok (NULL, " ");
		}

		if(!argv_complete){
			/* if not piping just write to argv */
			if(!is_piped){
				argv[argv_index] = tmp;
				argv_index++;
			} else { /* If piping Stop writing to argv and write to second argv */
				argv_pipe[argv_pipe_index] = tmp;
				argv_pipe_index++; 
			}

		}

		/* Move to next token in input */
		tmp = strtok (NULL, " ");
	}

}

int execute_cmd(char *cmd, char **argv){

	/* Generic Parent-Child vars */
	int status;
	pid_t child_pid;

	/* Pipe vars */
	char stdin_buffer[MAX_ARGS];
	pid_t pipe1_pid, pipe2_pid;
	int pfd[2];

	if(is_piped){ /* Separate conditional for running pipes */
		/* Open pipe */
		if (pipe(pfd) == -1){
			printf("Pipe failure");
			exit(1);
		}

		/* Fork first process */
		pipe1_pid = fork();

		if(pipe1_pid == 0){
			/* Write STDOUT to pipe */
			dup2(pfd[1], STDOUT_FILENO);

			/* Close both ends of pipe */
			close(pfd[0]);
			close(pfd[1]);

			/* Exec first half of command */
			execvp(cmd, argv);	
		}

		/* Fork second process */
    	pipe2_pid = fork();

		if (pipe2_pid == 0) {
			/* Read to pipe from STDIN */
			dup2(pfd[0], STDIN_FILENO);

			/* Close both ends of pipe */
			close(pfd[1]);
			close(pfd[0]);

			/* Exec second command */
			execvp(argv_pipe[0], argv_pipe);

		}

		/* Close out both ends of the pipe */
		close(pfd[1]);
		close(pfd[0]);
		
		/* Wait for both processes to finish */
		/* If this is a background process don't wait */
		if(!background_process){ 
			waitpid(pipe2_pid, &status, 0);
			waitpid(pipe2_pid, &status, 0);
		}

	} else { /* Normal Child - Parent Command Process */

		/* Process ID */
		child_pid = fork();

		/* Fork child process */
		if (child_pid == 0){
			if((redirect_file[0]) == 0){ 	/* If no redirection just exec cmd */
				execvp(cmd, argv);
			} else { 						/* Redirect to file */
				/* Handle Redirects */
				if(strncmp(redirect_file[0], ">", 1) == 0 ){

					/* Open file_ptr */
					FILE *file_ptr = fopen(redirect_file[1],"w+");
					/* duplicate STDOUT to file */
					dup2(fileno(file_ptr), STDOUT_FILENO);
					fclose(file_ptr);

					/* exec cmd */
					execvp(cmd, argv);
					
				} else if(strncmp(redirect_file[0], "<", 1) == 0 ){
					/* Open file_ptr */
					FILE *file_ptr = fopen(redirect_file[1],"r");
					
					/* duplicate file to STDIN*/
					dup2(fileno(file_ptr), STDIN_FILENO);
					fclose(file_ptr);

					/* exec cmd */
					execvp(cmd, argv);
				}

			} /* End Redirect handling conditional */ 

		} else if(!background_process){ /* parent */
				waitpid(child_pid, &status, 0); /* wait for child process to complete */
		}

	}

	return 0;
}

int main(){
	
	char input[100];
	char *argv[MAX_ARGS];
	char current_directory [MAX_PATH_CHAR_CNT];

	while(1){
		
		/* Usage: char *getcwd(char *buf, size_t size); */
		getcwd(current_directory, sizeof(current_directory));

		/* Clear out argv, argv_pipe, redirect, and global flags */
		memset(argv, 0, sizeof(argv));
		memset(argv_pipe, 0, sizeof(argv_pipe));
		memset(redirect_file, 0, sizeof(redirect_file));
		background_process = 0;
		is_piped = 0;

		/* Print Shell */
		printf("\n%s$ ", current_directory);
		
		/* Get user input */
		fgets(input,sizeof(input),stdin);

		/* Remove trailing newline */
		input[strlen(input)-1] = '\0';

		if (strncmp(input,"dir", 3) == 0 ){ 			/* Working */
			/* Parse command line input */
			parse_cmd(input, "ls", argv);
			execute_cmd("ls", argv);

		} else if (strncmp(input, "del", 3) == 0 ) { 	/* Working */
			/* Parse command line input */
			parse_cmd(input, "rm", argv);
			execute_cmd("rm", argv);

		} else if (strncmp(input, "copy", 4) == 0 ) {	/* Working */
			/* Parse command line input */
			parse_cmd(input, "cp", argv);
			execute_cmd("cp", argv);

		} else if ((strncmp(input, "move", 4) == 0 ) 
			|| (strncmp(input, "rename", 6) == 0 )) { 	/* Working */
			/* Parse command line input */
			parse_cmd(input, "mv", argv);
			execute_cmd("mv", argv);

		} else if (strncmp(input, "type", 4) == 0 ) { 	/* Working */
			/* Parse command line input */
			parse_cmd(input, "cat", argv);
			execute_cmd("cat", argv);

		} else if (strncmp(input, "cd", 2) == 0 ) { 	/* Working */
			/* Built-in shell command */
			parse_cmd(input, "cd", argv);
			chdir(argv[1]);

		} else if (strncmp(input, "more", 4) == 0 ) {	/* Working */
			/* Parse command line input */
			parse_cmd(input, "more", argv);
			execute_cmd("more", argv);

		} else if (strncmp(input, "md", 2) == 0 ) { 	/* Working */
			/* Parse command line input */
			parse_cmd(input, "mkdir", argv);
			execute_cmd("mkdir", argv);

		} else if (strncmp(input, "rd", 2) == 0 ) { 	/* Working */
			/* Parse command line input */
			parse_cmd(input, "rmdir", argv);
			execute_cmd("rmdir", argv);

		} else if (strncmp(input, "cls", 3) == 0 ) { 	/* Working */
			/* Parse command line input */
			parse_cmd(input, "clear", argv);
			execute_cmd("clear", argv);

		} else if (strncmp(input, "sort", 4) == 0 ) { 	/* Created to test Input redirect < */
			/* Parse command line input */
			parse_cmd(input, "sort", argv);
			execute_cmd("sort", argv);

		} else if (strncmp(input, "exit", 4) == 0 ) { 	/* Working */
			exit(1);
		} else { 
			/* Handle all other generic unix commands */
			parse_cmd(input, "GENERIC_CMD", argv);
			execute_cmd(argv[0], argv);
		}

	}

	return 0;
}