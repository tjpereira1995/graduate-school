/* Tyler Pereira 	*/
/* 2/18/2019		*/
/* CIS-570			*/
/* Compiled using gcc -g long_term_assignment_1.c -o long_term_prjct_1	*/
/* Can then run with ./long_term_prjct_1 								*/

/* References
*  Directory Info: http://pubs.opengroup.org/onlinepubs/009604599/functions/opendir.html
*		- http://pubs.opengroup.org/onlinepubs/7908799/xsh/dirent.h.html 
*
*  Fork & PID: https://ece.uwaterloo.ca/~dwharder/icsrts/Tutorials/fork_exec/
*		- Class slides Week 2
*
*  Handling Redirection & Pipes: Saw suggestions of using dup system command
*	- Class slides Week 2 - slide 41 Unix Pipes and Sockets
*
*  Strtok - https://www.tutorialspoint.com/c_standard_library/c_function_strtok.htm
*		- Used this to "split" the cmd string. Have used split in other languages but 
*		this built in C function provides the next instance/token vs returning a split array
*/