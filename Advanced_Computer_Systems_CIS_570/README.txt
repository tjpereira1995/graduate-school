Assignment 1: Consumers Producers
	- Found out that Segmentation Fualts when compiling and running on Mac OSX 
	was due to semaphore support on Mac OSX
	- Decided to compile in an online GCC compiler running on a remote linux enviromnent that supports sem_init
	- References:
		- Week 3 Lecture Notes detailing producer/consumer implementation
		- http://man7.org/linux/man-pages/man3/sem_wait.3.html
		- http://man7.org/linux/man-pages/man3/sem_post.3.html
		- http://man7.org/linux/man-pages/man3/pthread_create.3.html

Assignment 2: UDP/TCP File transfer
	- Have experience with this using sockets in python programming so decided to go that route
	- Compiled with gcc -g <input_src_file> -o <output_executbale>
	- References:
		- https://www.gnu.org/software/libc/manual/html_node/Host-Names.html
		- https://www.geeksforgeeks.org/socket-programming-cc/
		- http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html

Assignment 3: RPC Chat System
	- Implemented using sockets
	- Compiled with gcc -g <input_src_file> -o <output_executbale>
	- References:
		- https://www.gnu.org/software/libc/manual/html_node/Host-Names.html
		- https://www.geeksforgeeks.org/socket-programming-cc/
		- http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html