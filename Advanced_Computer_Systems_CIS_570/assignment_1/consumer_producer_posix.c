#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

/* specife type for project */
typedef int buffer_item;

/* buffer size, used 5 based on phlosophhers diagram in lectures */
#define BUFFER_SIZE 5

sem_t full;  								/* buffer is full if 0 			*/
sem_t empty; 								/* buffer is empty if 0 		*/
pthread_mutex_t mutex_lock;					/* mutex lock via POSIX Pthread */

buffer_item circular_buffer[BUFFER_SIZE];	/* circular buffer array 		*/
int buffer_index = 0;						/* to track insert/remove items */
int alternative_counter = 0;				/* track every other producer 	*/

/* function to insert item into buffer and incrmeent buffer_index */
void insert_item(buffer_item item)
{
    if (buffer_index < BUFFER_SIZE) {
    	printf("Inserting item %d at index %d\n", item, buffer_index+1);
        circular_buffer[buffer_index++] = item;
    } else {
        printf("Buffer is Full!!!\n");
    }
}
 
/* function to remove last item in buffer and decrement buffer_index */
void remove_item(int index)
{
    if (buffer_index > 0) {
    	printf("Removing item %d at index %d\n", circular_buffer[buffer_index], buffer_index);
        circular_buffer[--buffer_index];
    } else {
        printf("Buffer is Empty!!!\n");
    }
}

void producer()
{
	do  {
		/* Every other producer call insert item */
		if (alternative_counter % 2 == 0){
		 /* Sleep for random time up to 5 seconds Max */
			sleep(rand() % 5);
		} else {
			/* Generate item to be added with max of 100 */
        	buffer_item item = rand() % 100;	
        
		    /* decrement empty semaphore */
			sem_wait(&empty);

			/* lock mutex for critical section */
			pthread_mutex_lock(&mutex_lock);

			/* Critical Section - insert the item to the  buffer */
		    insert_item(item);

		    /* Crit section completed release the mutex lock*/
			pthread_mutex_unlock(&mutex_lock);

			/* increment full semaphore */
			sem_post(&full); 
	}

	/* increment counter for every other producer call hadnling */
	alternative_counter++;

	} while (1);

}
   
void consumer()
{
	do {
		 /* Sleep for random time up to 5 seconds Max */
		sleep(rand() % 5);
		
		/* decrement full semaphore */
		sem_wait(&full);

		/* lock mutex for critical section */
		pthread_mutex_lock(&mutex_lock);

		/* Critical Section - remove the item from the buffer */
        remove_item(buffer_index);

        /* Crit section completed release the mutex lock*/
		pthread_mutex_unlock(&mutex_lock);

		/* increment empty semaphore */
		sem_post(&empty);

	} while (1);

}

/* Tyler Pereira 
** CIS-570
** Compile using gcc
** References 
**	- Week 3 Lecture Notes
** 	- http://man7.org/linux/man-pages/man3/sem_wait.3.html
**  - http://man7.org/linux/man-pages/man3/sem_post.3.html
**  - http://man7.org/linux/man-pages/man3/pthread_create.3.html
**
**  OSX handling of smeaphores is different to standard linux
**  Creating this build for generic linux and compiling in an onlid compiler
*/	

int main(int argc, char* argv[])
{

	/* Get command line args */
	int sleep_time 	= atoi(argv[1]);
    int producers 	= atoi(argv[2]);
    int consumers 	= atoi(argv[3]);

	/* Init mutex lock */
    pthread_mutex_init(&mutex_lock, NULL);

    /* Init full to 0 and empty to BUFFER_SIZE */
    sem_init(&full, 0, 0);
    sem_init(&empty, 0, BUFFER_SIZE);

    /* Init pthread */
    pthread_t thread;

    /* Create producer threads */
    int producer_count=0;
    for (producer_count=0; producer_count < producers; producer_count++) {
        pthread_create(&thread, NULL, producer, NULL);
        printf("Producer created\n");
    }

    /* Create consumer threads */
    int consumers_count=0;
    for (consumers_count=0; consumers_count < consumers; consumers_count++) {
        pthread_create(&thread, NULL, consumer, NULL);
        printf("Consumer created\n");
    }

    /* Main wait itme for consumer producers */
    sleep(sleep_time);

	return 0;
}