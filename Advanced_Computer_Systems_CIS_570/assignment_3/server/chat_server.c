#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <errno.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <string.h> 

#define PORT 8080 /* alternative to port 80 which is commonly used */

/* Tyler Pereira
** 3/31/2019
** CIS 570
** References
** 		- https://www.gnu.org/software/libc/manual/html_node/Host-Names.html
** 		- https://www.geeksforgeeks.org/socket-programming-cc/
** 		- http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html
*/

int main() 
{ 
	char server_name[256]; struct hostent *server_info; 
    char *ip_addr; 

    /* Get Host Name */ 
    gethostname(server_name, sizeof(server_name)); 
  
    /* get Host Info */
    server_info = gethostbyname(server_name); 
  
    /* Get IP receiver_addr of both sender and reciever since simulating on single machine*/
    /* IF two seprate machines in use could set static IP on receiving machine or set specific host name */
    /* To connect to receiver socket */
    ip_addr = inet_ntoa(*((struct in_addr*) server_info->h_addr_list[0]));


	int server_file_descriptor;				/* status of socket file descriptor */
	int socket_sts;							/* status of socket connection */
	int read_sts; 							/* status of reading from socket */

	struct sockaddr_in server_addr; 		/* socket adddr */
	int addrlen = sizeof(server_addr); 		/* length fo socket addr */
	
	/* create socket */
	server_file_descriptor = socket(AF_INET, SOCK_STREAM, 0);
	
	/* set socket IP addr and port */
	server_addr.sin_family = AF_INET; 
	inet_pton(AF_INET, ip_addr, &server_addr.sin_addr); 
	server_addr.sin_port = htons( PORT ); 
	
	/* bind socker to port 8080 */
	bind(server_file_descriptor, (struct sockaddr *)&server_addr, sizeof(server_addr));

	/* begin listening on socket, max of 5 concurrent connections */
	listen(server_file_descriptor, 5);

	/* loop so that socket never closes and continues to register new clients */
	int client_num = 0; /* Counter to keep Track of client */

	printf("RPC Chat System\n");
	while((socket_sts = accept(server_file_descriptor, (struct sockaddr *)&server_addr, (socklen_t*)&addrlen))>0){
		
		int pid; 			/* process id */
		client_num++; 		/* increment client num */
		
		/* Send message letting client know it is connected and the user num */
		char init_message[128];
		snprintf(init_message, sizeof(init_message), 
			"Connected to the RPC Socket Chat system as Client %d\n", client_num);
		send(socket_sts , init_message , strlen(init_message) , 0 );
		printf("%s\n", init_message);

		char message[128]; /* init message buffer */

		/* fork a new child process for this socket */
	    if((pid = fork()) == 0) {
	    	/* keep receiving message until child process exits*/
	        while (recv(socket_sts, message, 128, 0)>0) { 

	        	/* if user enters exit kill process and close socket*/
	        	if(strncmp(message, "exit", 4) == 0 ){
	        		
	        		/* Print client has left chat */
	        		printf("Client %d has left the chat\n", client_num);

	        		/* Close the socket */
	        		shutdown(server_file_descriptor, 0);
	        		exit(0);
	        	} else { /* Print user message */
	        		
	        		printf("User %d: %s\n", client_num, message);
	        		/* clear message buffer */
	            	memset(message, 0, sizeof(message));
	        	}
	                      
	        }
	    }

	}

	return 0; 
} 



