#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <errno.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <string.h> 
#include <time.h>

#define PORT 8080 /* alternative to port 80 which is commonly used */

/* Tyler Pereira
** 3/31/2019
** CIS 570
** References
** 		- https://www.gnu.org/software/libc/manual/html_node/Host-Names.html
** 		- https://www.geeksforgeeks.org/socket-programming-cc/
** 		- http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html
** 		- https://www.codingame.com/playgrounds/14213/how-to-play-with-strings-in-c/string-split
*/
int main() 
{ 	    
    /* Get host info */
    char client_host_name[256]; struct hostent *client_host_info; 
    char *client_ip_addr; 

    /* Get Host Name */ 
    gethostname(client_host_name, sizeof(client_host_name)); 
  
    /* get Host Info */
    client_host_info = gethostbyname(client_host_name); 
  
    /* Get IP Address of both sender and reciever since simulating on single machine*/
    /* IF two seprate machines in use could set static IP on receiving machine or set specific host name */
    /* To connect to receiver socket */
    client_ip_addr = inet_ntoa(*((struct in_addr*) client_host_info->h_addr_list[0])); 

	int sock_result = 0; 

	struct sockaddr_in client_addr; 

	/* create socket */
	sock_result = socket(AF_INET, SOCK_STREAM, 0);

	memset(&client_addr, '0', sizeof(client_addr)); 

	/* set socket IP addr and port */
	client_addr.sin_family = AF_INET;
	inet_pton(AF_INET, client_ip_addr, &client_addr.sin_addr);
	client_addr.sin_port = htons(PORT); 
	
	/* connect sender to socket */
	connect(sock_result, (struct sockaddr *)&client_addr, sizeof(client_addr));

	/* Receive init message after connection */
	char init_message[128];
	read(sock_result , init_message, 1024);
	printf("%s\n", init_message);

	/* begin loop to prompt for message */
	char message[128];
	while(1) {
		/* Prompt and Get user input */
		printf("Send Message:  ");
		fgets(message, 128, stdin);

		/* Send message over socket */
		send(sock_result, message, strlen(message), 0);

		/* if user enters exit, kill program */
		if(strncmp(message, "exit", 4) == 0 ){
			printf("Exiting Chat System.\n");
			exit(0);
		}
		memset(message, 0, sizeof(message));
	}

	return 0; 
} 
