// Tyler Pereira
// CIS 580 F# Homework 
// 3/6/2020

let max list =
  match list with
    | head :: tail ->                               // Non-Empty List w Head & Tail
      let rec loopMax cur_max list =                // Nested function to keep track of Max
        match list with     
          | [] -> cur_max                           // Match empty list return latest Max
          | head :: tail ->                         // Match non-empty list
            if head < cur_max then                  // If head < max then recursivley run
              loopMax cur_max tail                  // function passing keeping old max 
            else
              loopMax head tail                     // Else, recurse function with new max
      loopMax head tail                             // return result of nested function 

let rec nth n list =
  match list with
    | [x] -> x                                      // If list is only 1 element, return element
    | head :: _ when n = 0 -> head                  // If n == 0 then return head of list
    | _ :: tail -> nth (n-1) tail                   // Recurse function and decrement n by 1
                                                    // Once n ==0 we have reached the element

let rec zip t =                             
  match t with                                      // Pattern match tuple
    | (x,y) -> 
      let rec nest l1 l2 zipped_list =              // Make nested function to 
        match l1, l2 with                           // Keep track of list
          | [], [] -> zipped_list                   // If both lists, empty return tracked list
          | h1::t1,h2::t2 ->                        // Else, recurse function passing tails and tracked list
            nest t1 t2 (zipped_list @ [(h1,h2)])
      nest x y []                                   // Return result of nested function

let rec search f list =
  match list with                                   // If list empty, return -1
    | [ ] -> -1 
    | head::tail ->                                 // Otherwise, make new function to test each list element
    let rec test n list =                 
      match list with                       
      | [ ] -> -1                                   // If list is empty return -1 (All tests failed)
      | head::tail -> 
        if f head then n                            // If test passed so return element n
        else test (n+1) (List.tail list)            //  Else recurse function passing and 
    test 0 list                                     // increment n counter

printfn "Testing max function"
printfn "max [89; 42; 112; 36; 90; -205] : %d" (max [89; 42; 112; 36; 90; -205])
printfn "max [-5; 0; -12; -3] : %d" (max [-5; 0; -12; -3])
printfn "max [c; a; p; f] : %s " (max ["c"; "a"; "p"; "f"])

printfn "\nTesting nth function"
printfn "nth 2 [0; 1; 2; 3; 4] : %d" (nth 2 [0; 1; 2; 3; 4])
printfn "nth 3 [cat; dog; bird; fox] : %s"  (nth 3 ["cat"; "dog"; "bird"; "fox"])
printfn "nth 1 [cat; dog; bird; fox] : %s" (nth 1 ["cat"; "dog"; "bird"; "fox"])

printfn "\nTesting zip function"
printfn "zip ([1], [2]) : %A" (zip ([1], [2]))
printfn "zip ([a; b; c; d; e], [1; 2; 3; 4; 5]) : %A" (zip (["a"; "b"; "c"; "d"; "e"], [1; 2; 3; 4; 5])) 

printfn "\nTesting search function"
printfn "search (fun x -> x > 10) [ 2; 12; 3; 23; 62; 8; 2 ] : %d" (search (fun x -> x > 10) [ 2; 12; 3; 23; 62; 8; 2 ])
printfn "search (fun x -> xmod2 = 0) [ 3; 17; 9; 23; 62; 8; 2 ] : %d" (search (fun x -> x%2 = 0) [ 3; 17; 9; 23; 62; 8; 2 ])
printfn "search (fun s -> s < horse) [ pig; lion; horse; cow; elephant; turkey] :%d" (search (fun s -> s < "horse") [ "pig"; "lion"; "horse"; "cow"; "elephant"; "turkey" ])
printfn "search (fun n -> n = 5) [ 3; 7; 12; 6 ] : %d" (search (fun n -> n = 5) [ 3; 7; 12; 6 ])


// Output
// Testing max function
// max [89; 42; 112; 36; 90; -205] : 112
// max [-5; 0; -12; -3] : 0
// max [c; a; p; f] : p 

// Testing nth function
// nth 2 [0; 1; 2; 3; 4] : 2
// nth 3 [cat; dog; bird; fox] : fox
// nth 1 [cat; dog; bird; fox] : dog

// Testing zip function
// zip ([1], [2]) : [(1, 2)]
// zip ([a; b; c; d; e], [1; 2; 3; 4; 5]) : [("a", 1); ("b", 2); ("c", 3); ("d", 4); ("e", 5)]

// Testing search function
// search (fun x -> x > 10) [ 2; 12; 3; 23; 62; 8; 2 ] : 1
// search (fun x -> xmod2 = 0) [ 3; 17; 9; 23; 62; 8; 2 ] : 4
// search (fun s -> s < horse) [ pig; lion; horse; cow; elephant; turkey] :3
// search (fun n -> n = 5) [ 3; 7; 12; 6 ] : -1