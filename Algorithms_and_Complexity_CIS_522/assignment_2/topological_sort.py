# Tyler Pereira
# CIS 522
# Problem 3.3
from collections import defaultdict

class Graph():
    def __init__(self, vertices):
        self.graph = defaultdict(list)
        self.V = vertices

    def addEdge(self, u, v):
        self.graph[u].append(v)
    
    def removeEdge(self, u, v):
        self.graph[u].remove(v)
    
    # Get all incoming edges for each node
    def getIncoming(self):
        incoming_edges = [0] * self.V

        for idx, vertice in enumerate(self.graph):
          for edge in self.graph[vertice]:
            incoming_edges[edge] += 1
        
        return incoming_edges

    # Get last occurance of zero incoming 
    # Avoid anything from passed avoid arr    
    def return_last(self, arr, val, avoid):
      tmp = []
      for idx, num in enumerate(arr):
       if num == val:
         tmp.append(idx)
      
      res = list(set(tmp)^set(avoid))
      
      try:
         return res[-1]
      except:
        return -1

    def topologicalSort(self, stack ):
      # Get incoming edges for each node
      in_degrees = self.getIncoming()
      
      # get last instance of node incoming edges
      last = self.return_last(in_degrees, 0, stack)
      
      # If valid node found with no incoming edges 
      if (last != -1):
        stack.append(last)
        # Remove node from graph and add to a stack
        try:
          self.graph.pop(last)
        except:
          print("No outgoing edges from Node " + str(last))
        
        # Recurisvley call this function
        while (len(stack) < self.V):
          self.topologicalSort(stack)
      else: 
        # If we can't find a node that does not have an incoming edge
        print("Graph has a cycle. Nodes below are a part of a cycle.")
        for i in self.graph:
          print(i)    
        self.V = -1 # break base case
      
      return stack

### DRIVER / TEST CODE ###

print("Ex. No cycle Found ")       
g= Graph(4) 
g.addEdge(0, 1)
g.addEdge(1, 2) 
g.addEdge(2, 3) 
g.addEdge(1, 3) 

stack = []
stack = g.topologicalSort(stack)
print(stack)

print("\n\n")

print("Ex. Cycle found ") 
g2 = Graph(4)
g2.addEdge(0, 1)
g2.addEdge(1, 2)
g2.addEdge(2, 3)
g2.addEdge(3, 1)

stack = []
stack = g2.topologicalSort(stack)

"""
Python 3.7.4 (default, Jul  9 2019, 00:06:43)
[GCC 6.3.0 20170516] on linux
Ex. No cycle Found
No outgoing edges from Node 3
[0, 1, 2, 3]



Ex. Cycle found
Graph has a cycle. Nodes below are a part of a cycle.
1
2
3
"""