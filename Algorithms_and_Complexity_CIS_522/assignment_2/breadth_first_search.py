# Tyler Pereira
# CIS 522
# Problem 3.11

from collections import defaultdict

class NodeConnect():
    def __init__(self, name, time):
        self.name = name
        self.time = time
    
    def __str__(self):
        return str(self.__dict__)  

class Graph():
    def __init__(self, vertices, virus_pc, virus_time):
        self.graph      = defaultdict(list)
        self.V          = vertices
        self.virus_pc   = virus_pc
        self.virus_time = virus_time
    
    def addNode(self, u):
      self.graph[u] = []

    def addEdge(self, u, v, connect_time):
          connection = NodeConnect(v, connect_time)
          self.graph[u].append(connection)
    
    def removeEdge(self, u, v):
        self.graph[u].remove(v)

    def modified_bfs(self, first): 
        # Init empty dict and array to track result
        infected = {}
        bfs_result = []

        # Mark all not visited
        # Using a dict as I am using string as node label 
        visited = {}
        
        for node in self.graph:
          visited[node] = False
  
        # Create a queue for bfs
        queue = [] 
  
        # Mark node as visited
        queue.append(first) 
        
        visited[first] = True
        
        # Add infected PC to dict tracking 
        infected[self.virus_pc] = self.virus_time
  
        while queue: 
            # Dequeue current node and add to result
            curr_node = queue.pop(0) 
            bfs_result.append(curr_node)

            # Loop through adjacent nodes
            # If node hasnt visited mark it and add to queue
            for i in self.graph[curr_node]: 
                if visited[i.name] == False:
                    # Check if it could be infected
                    # Will be if infected current node
                    # time is less than this connection time 
                    try:
                      if (infected[curr_node] < i.time):
                        infected[i.name] = i.time
                    except:
                      print("")
                    # Append and set visited to true
                    queue.append(i.name) 
                    visited[i.name] = True
        
        return bfs_result, infected

def getTraceData(size, trace_data, cA, tA):
  # Init Graph
  g = Graph(size, cA, tA) 

  # Add Nodes
  for i in range(1, size+1):
    g.addNode('c' +str(i))

  # Add trace data
  for data in trace_data:
        g.addEdge(data[0], data[1], data[2])

  return g
  

def isEffected(size, trace_data, cA, tA, cB, tB):
  
  trace_data=getTraceData(size, trace_data, cA, tA)

  bfs, infected = trace_data.modified_bfs(cA)

  for v in bfs:
    try:
      print(v + " was infected at time " + str(infected[v]))
    except:
      print(v + " was not infected")


### DRIVER / TEST CODE ###
print("Test 1: Book Example ")
print("Expected Result: C3 is not infected")
trace_data = [
              ['c1', 'c4', 12], 
              ['c1', 'c2', 14],
              ['c2', 'c3', 8 ]
             ]

isEffected(4, trace_data, 'c1', 2, 'c3', 8)
print("\n")

print("Test 2: Set C3 higher than 14 Seconds")
print("Expected Result: C3 is infected")
trace_data = [
              ['c1', 'c4', 12], 
              ['c1', 'c2', 14],
              ['c2', 'c3', 16 ]
             ]

isEffected(4, trace_data, 'c1', 2, 'c3', 16)
print("\n")

print("Test 3: Set C2/C4 lower than C1 Seconds")
print("        Set C3 Higher than C2")
print("Expected Result: C1 is only machine infected")
print("Expected Result: C3 is infected")
trace_data = [
              ['c1', 'c4', 12], 
              ['c1', 'c2', 12],
              ['c2', 'c3', 16 ]
             ]

isEffected(4, trace_data, 'c1', 13, 'c2', 12)

"""
Python 3.7.4 (default, Jul  9 2019, 00:06:43)
[GCC 6.3.0 20170516] on linux
Test 1: Book Example
Expected Result: C3 is not infected
c1 was infected at time 2
c4 was infected at time 12
c2 was infected at time 14
c3 was not infected


Test 2: Set C3 higher than 14 Seconds
Expected Result: C3 is infected
c1 was infected at time 2
c4 was infected at time 12
c2 was infected at time 14
c3 was infected at time 16


Test 3: Set C2/C4 lower than C1 Seconds
        Set C3 Higher than C2
Expected Result: C1 is only machine infected
Expected Result: C3 is infected

c1 was infected at time 13
c4 was not infected
c2 was not infected
c3 was not infected
"""