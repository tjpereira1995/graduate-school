#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>

#define BUFFER_SIZE 8

struct msgBuff {
    long  type;
    char text[BUFFER_SIZE];
};

void send_msg(int qid, struct msgBuff *buffer, long type, char *text);
void read_msg(int qid, struct msgBuff *buffer, long type);
void delete_queue(int qid);
int create_queue();
int get_queue_id(); 

void delete_queue(int qid)
{
    msgctl(qid, IPC_RMID, 0);
}

int create_queue(key_t key)
{
    return msgget(key, IPC_CREAT|0666);    
}

int get_queue_id(key_t key)
{
    return msgget(key, 0400);
}

void send_msg(int qid, struct msgBuff *buffer, long type, char *text)
{
    buffer->type = type;
    strcpy(buffer->text, text);

    if ((msgsnd(qid, (struct msgBuff *)buffer, 
                sizeof(buffer->text)+1, 0)) < 0)
    {
        fprintf(stderr, "Error: Failed to send message\n");
        exit(-1);
    }

    fprintf(stdout, "Msg sent (%lu): %s\n", buffer->type, buffer->text);
}

void read_msg(int qid, struct msgBuff *buffer, long type)
{
    buffer->type = type;
    if (( msgrcv(qid, (struct msgBuff *)buffer, 
         sizeof(buffer->text)+1, buffer->type, IPC_NOWAIT)) < 0)
    {
        perror("msgrcv");
        exit(-1);
    }

    fprintf(stdout, "Msg received (%lu): %s\n", buffer->type, buffer->text); 
}


int main(int argc, char *argv[])
{
    int msg_qid;
    key_t key;
    struct msgBuff q_buffer;

    if(argc == 1)
    {
        fprintf(stderr, "Error: No args passed.\n");
        return -1;
    }
    if (strcmp(argv[1], "-c") == 0 || strcmp(argv[1], "-C") == 0 )
    {
        /* Parse cmd line args */
        if (argc != 3)
        {
             fprintf(stderr, "Error: Bad Call\n");
             fprintf(stderr, "Usage: msgQTest -c/C <key>\n");
             return -1;
        }

        key = atoi(argv[2]);

        msg_qid = create_queue(key);
        if (msg_qid == -1)
        {
            fprintf(stderr, "Error: Failed to create queue\n");
        }
    }

    else if (strcmp(argv[1], "-d") == 0 || strcmp(argv[1], "-D") == 0 )
    {
        /* Parse cmd line args */
        if (argc != 3)
        {
             fprintf(stderr, "Error: Bad Call\n");
             fprintf(stderr, "Usage: msgQTest -d/D <key>\n");
             return -1;
        }

        key = atoi(argv[2]);

        msg_qid = get_queue_id(key);
        if (msg_qid == -1)
        {
            fprintf(stderr, "Error: Failed to get queue id\n");
        }

        /* delete queue */
        delete_queue(msg_qid);
    }

    else if (strcmp(argv[1], "-s") == 0 || strcmp(argv[1], "-S") == 0 )
    {
        /* Parse cmd line args */
        if (argc != 5)
        {
             fprintf(stderr, "Error: Bad Call\n");
             fprintf(stderr, "Usage: msgQTest -s/S <key> <type> <text>\n");
             return -1;
        }

        key = atoi(argv[2]);

        msg_qid = get_queue_id(key);
        if (msg_qid == -1)
        {
            fprintf(stderr, "Error: Failed to get queue id\n");
        }

        /* setup buffer */
        q_buffer.type = atol(argv[3]);
        strcpy(q_buffer.text, argv[4]);

        /* send msg to queue */
        send_msg(msg_qid, &q_buffer, q_buffer.type, q_buffer.text);
    }

    else if (strcmp(argv[1], "-r") == 0 || strcmp(argv[1], "-R") == 0 )
    {
        /* Parse cmd line args */
        if (argc != 4)
        {
             fprintf(stderr, "Error: Bad Call\n");
             fprintf(stderr, "Usage: msgQTest -r/R <key> <type>\n");
             return -1;
        }

        key = atoi(argv[2]);

        msg_qid = get_queue_id(key);
        if (msg_qid == -1)
        {
            fprintf(stderr, "Error: Failed to get queue id\n");
        }

        /* setup buffer */
        q_buffer.type = atol(argv[3]);

        /* send msg to queue */
        read_msg(msg_qid, &q_buffer, q_buffer.type);
    }

    return 0;
}
