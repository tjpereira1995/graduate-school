#include <unistd.h>
#include <fcntl.h>

void myPrint(char *msg)
{
    char *c = msg;
    while ( *c)
    {
        write(STDOUT_FILENO, c, 1);
        *c++;
    }
}

int myStrCmp(char *s1, char *s2)
{
    char *c1 = s1;
    char *c2 = s2;

    while (*c1 || *c2)
    {
        if (*c1 != *c2)
            return 0;
        *c1++; *c2++;
    }

    return 1;
}

int main(int argc, char *argv[])
{
    int fd[3];
    char buf1[32], buf2[32];

    /* Chech for correct num of arguments */
    if (argc != 5)
    {
        myPrint("Error.\n  Usage: ./test02 <-s|-e> <file1> <file2> <file3>\n");
        return -1;
    }

    /* Open & Create files */
    fd[0] = open(argv[2], O_RDONLY);
    fd[1] = open(argv[3], O_RDONLY);
    fd[2] = open(argv[4], O_CREAT | O_RDWR | O_TRUNC, 0700);

    /* Read in contents of file1 and file2 */
    read(fd[0], &buf1, sizeof(buf1));
    read(fd[1], &buf2, sizeof(buf2));

    /* Check for argument */
    if (myStrCmp(argv[1], "-e"))
    {
        write(fd[2], &buf2, sizeof(buf2));
        write(fd[2], &buf1, sizeof(buf1));

        myPrint(argv[4]);
        myPrint(" contains:\n");
        myPrint("\t"); myPrint(buf2);
        myPrint("\t"); myPrint(buf1);
    }
    else if (myStrCmp(argv[1], "-s"))
    {
        write(fd[2], &buf1, sizeof(buf1));
        write(fd[2], &buf2, sizeof(buf2));

        myPrint(argv[4]);
        myPrint(" contains:\n");
        myPrint("\t"); myPrint(buf1);
        myPrint("\t"); myPrint(buf2);

    }
    else
    {
        myPrint("Error.\n  Usage: ./test02 <-s|-e> <file1> <file2> <file3>\n");
    }

    close(fd[0]);
    close(fd[1]);
    close(fd[2]);

    return 0;
}
