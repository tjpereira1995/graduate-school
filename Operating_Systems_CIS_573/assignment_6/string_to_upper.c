#include <unistd.h>

char myToUpper(char someChar)
{
     if ( (someChar >= 'a') && (someChar <= 'z') )
     {
         someChar = someChar - 32;
     }

     return someChar;
}

int main(int argc, char *argv[])
{
    char input; 

    for(;;)
    {
        /* Use read sys call to get 1 byte char from user */
        if ( read(STDIN_FILENO, &input, sizeof(char)) < 0)
        {
            return -1;
        }

        /* Convert to upper */
        input = myToUpper(input);

        /* Print to stdout using write sys call */
        if ( write(STDOUT_FILENO, &input, sizeof(char)) < 0)
        {
            return -1;
        }
    }

    return 0;
}
