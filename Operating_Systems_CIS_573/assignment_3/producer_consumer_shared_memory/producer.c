#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include "buffer.h"

int main(int argc, char *argv[])
{
    char input;
    const int buffer_size = sizeof(bufferStruct);
    int shm_fd;
    bufferStruct *shm_ptr;

    /* Open shared memory for r/w since & create */
    shm_fd = shm_open(BUFFER_SHM_NAME, O_CREAT | O_RDWR, 0666);

    /* Set size of shm */
    ftruncate(shm_fd, buffer_size);

    /* Map shm pointer */
    shm_ptr = mmap(0, buffer_size, PROT_WRITE, MAP_SHARED, shm_fd, 0);

    /* Scan for user input and produce */
    while (input != '0')
    {
        scanf("%c", &input);
        if (input == '0')
            break;
        else if (input == '\n')
            producer(shm_ptr);
        else
            continue;
    }

    /* shm cleanup */
    shm_unlink(BUFFER_SHM_NAME);
    close(shm_fd);

    return 0;
}
