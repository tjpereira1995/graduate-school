#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define MAX_SIZE 8
#define BUFFER_SHM_NAME "BUFFER_SHM"

typedef struct bufferStruct
{
    int in;
    int out;
    int content[MAX_SIZE];
} bufferStruct;

void printBuffer(bufferStruct *buffer)
{
    int i;
    i = buffer->out;
    while(i != buffer->in)
    {
        fprintf(stdout, "%d ", buffer->content[i]);
        i = (i + 1) % MAX_SIZE;
    }
    fprintf(stdout, "\n");
}

void producer(bufferStruct *buffer)
{
    int next_produced = rand() % 100;

    if ((buffer->in + 1) % MAX_SIZE != buffer->out) 
    {
        buffer->content[buffer->in] = next_produced;
        buffer->in = (buffer->in + 1) % MAX_SIZE;
    }
    else
    {
        fprintf(stdout, "    Buffer is full ...\n");
    }
    printBuffer(buffer);
}

void consumer(bufferStruct *buffer)
{
    int next_consumed;
    if (buffer->in != buffer->out)
    {
        next_consumed = buffer->content[buffer->out];
        buffer->out = (buffer->out +1) % MAX_SIZE;

        printf("%d is consumed\n", next_consumed);
    }
    else
    {
        fprintf(stdout, "    Buffer is empty...\n");
    }
    printBuffer(buffer);
}
