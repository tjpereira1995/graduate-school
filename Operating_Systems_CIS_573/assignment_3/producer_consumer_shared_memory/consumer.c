#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include "buffer.h"

int main(int argc, char *argv[])
{
    char input;
    const int buffer_size = sizeof(bufferStruct);
    int shm_fd;
    bufferStruct *shm_ptr;

    /* Open shared memory for r/w since need to update out 
     * but not for creating.
     */
    shm_fd = shm_open(BUFFER_SHM_NAME, O_RDWR, 0666);

    /* Map shm pointer */
    shm_ptr = mmap(0, buffer_size, PROT_WRITE, MAP_SHARED, shm_fd, 0);

    /* Scan for user input and consume */
    while (input != '0')
    {
        scanf("%c", &input);
        if (input == '0')
            break;
        else if (input == '\n')
            consumer(shm_ptr);
        else
            continue;
    }

    /* shm cleanup */
    close(shm_fd);

    return 0;
}
