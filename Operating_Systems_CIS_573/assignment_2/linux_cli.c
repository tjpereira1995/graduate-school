#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

#define MAX_LINE 80 /* The max len of command */

int main(int argc, char **argv, char **envp)
{
    int i;
    int status;
    pid_t child_pid;
    int background_task = 0;
    int should_run = 1;
    
    char input[128];
    char *args[MAX_LINE/2 + 1];
    char *token;

    while(should_run)
    {
        /* Reset backgound flag */
        background_task = 0;

        /* Get user input */
        printf("osh>");
        fgets(input, sizeof(input), stdin);
        fflush(stdout);

        /* Remove trailing newline from fgets */
        input[strlen(input) - 1] = '\0';

        /* Continue if no command entered */
        if (strlen(input) == 0)
        {
            continue;
        }

        /* Grab command, split by space */
        token = strtok(input, " ");

        for(i = 0; token != NULL; i++)
        {   
            args[i] = token;
            token = strtok(NULL, " ");
        }
        
        /* Check for & for background cmd */
        if (strcmp(args[i-1], "&") == 0)
        {    
            background_task = 1;
            args[i-1] = NULL;
        } 
        else
        {   /* Otherwise set last element to NULL for execvp */
            args[i] = NULL;
        }

        /* Check if exit entered */
        if (strcmp(args[0],"exit") == 0)
        { 
            should_run = 0;
        }

        /* Fork and run command in child process */
        if ((child_pid = fork()) < 0)
	{
	    printf("Error: Failed to fork child proc\n");
	}
	else if (child_pid == 0)
	{
	    /* Child Proc */
            /* Reset parent if there is a background task */
            if (background_task == 1)
                setpgid(0,0);

            execvp(args[0], args);
	}
	else
	{
	    /* Parent Proc */
            if (background_task == 0)
	        while(wait(&status) != child_pid);
            else
            {
                printf("[1]%d\n",getpid());
            }
	}
    }

    return EXIT_SUCCESS;
}
