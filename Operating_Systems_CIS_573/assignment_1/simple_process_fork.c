#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv, char **envp)
{
    printf("%d *\n", getpid());
    fork();
    printf("%d **\n", getpid());
    fork();
    printf("%d ***\n", getpid());

    /* Part b */
    fork();
    printf("%d ****\n", getpid());

    /* Part d */
    fork();
    printf("%d *****\n", getpid());

    fork();
    printf("%d ******\n", getpid());

    sleep(10);

    return EXIT_SUCCESS;
}
