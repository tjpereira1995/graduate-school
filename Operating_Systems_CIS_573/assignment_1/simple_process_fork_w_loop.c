#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

#define NUM_CHILDREN (5)

int main(int argc, char **argv, char **envp)
{
    pid_t pids[NUM_CHILDREN];
    pid_t child_pid;
    int i, j;
    int status;

    /* Implement looping instead of 5 seperate fork() calls */
    for (i = 0; i < NUM_CHILDREN; i++)
    {
        if ((pids[i] = fork()) < 0) 
        {
            fprintf(stderr, "Fork error\n");
            exit(-1);
        }
        else if(pids[i] == 0) 
        {
            printf("%d ", getpid());
            for (j = 0; j < i; j++)
                printf("*");
            printf("\n");
            sleep(1);
        }
    }

    /* Wait for children to finish */
    while((child_pid = wait(&status)) > 0)
        printf("Process %d Exited\n", child_pid);

    return EXIT_SUCCESS;
}
