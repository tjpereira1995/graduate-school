#include <stdio.h>
#include <string.h>

#define MAX_NUM_OF_ARGS 5
#define MAX_ARG_SIZE 256

static char argumentArray[MAX_NUM_OF_ARGS][MAX_ARG_SIZE];

int main(int argc, char *argv[])
{
    int i = 0;
    int count = 0;

    /* Pre populate with defaultArg */
    while(count < MAX_NUM_OF_ARGS)
    {
        strcpy(argumentArray[count], "defaultArgs");
        count++;
    }

    /* Copy argv to argumentArray and print args */
    while (argv[i] != NULL)
    { 
        /* Check for too many args */
        if (i >= 5)
        {
            printf("Usage : ./MyCommand2 Arg1 Arg2 ... \n");
            printf("  Your arguments exceeded the maximum number of arguments (4)\n");
            return -1;
        }

        /* Copy in passed args */
        strcpy(argumentArray[i],argv[i]);

        i++;
    }

    /* Print result */
    for (i = 0; i < MAX_NUM_OF_ARGS; i++)
        printf("argumentArray[%d]: %s\n", i, argumentArray[i]);
    return 0;
}
