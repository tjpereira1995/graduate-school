#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <string.h>

void sig_clock(int signum){
    time_t rawtime;
    struct tm * timeinfo;

    /* Get locak time */
    time ( &rawtime );
    timeinfo = localtime ( &rawtime );

    /* Remove newline from time string */
    char *time_str = asctime(timeinfo);
    strtok(time_str, "\n");

    /* Print out time and overwrite terminal output */
    printf(" %s \r",  time_str );
    fflush(stdout);
}
 
int main()
{
    signal(SIGALRM, sig_clock); /* Register signal */
 
    for(;;)
    {
        alarm(1);  /* Sched alarm */
        sleep(1);  /* Delay 1 sec */
    }

    return 0;
}