/*
 * References 
 *  - Week 7 Lecture Notes
 *  - Week 5 Part 1
 *  - http://man7.org/linux/man-pages/man3/sem_wait.3.html
 *  - http://man7.org/linux/man-pages/man3/sem_post.3.html
 *  - http://man7.org/linux/man-pages/man3/pthread_create.3.html
 */  

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#define MAX_LENGTH (10)

sem_t full;                         /* buffer is full if 0          */
sem_t empty;                        /* buffer is empty if 0         */
pthread_mutex_t mutex_lock;         /* mutex lock via POSIX Pthread */
int num_items;                      /* num items to produce         */

typedef struct bufferStruct
{
    int in;
    int out;
    int content[];
} bufferStruct;

void printBuffer(bufferStruct *buffer)
{
    int i;
    i = buffer->out;
    while(i != buffer->in)
    {
        fprintf(stdout, "%d ", buffer->content[i]);
        i = (i + 1) % num_items;
    }
}

void *producer(void *arg)
{
    bufferStruct *buffer = arg;
    do {
        /* decrement empty semaphore */
        sem_wait(&empty);

        /* lock mutex for critical section */
        pthread_mutex_lock(&mutex_lock);

        int next_produced = rand() % 100;
        if ((buffer->in + 1) % num_items != buffer->out) 
        {
            buffer->content[buffer->in] = next_produced;
            printf("Producer: Produced %d to   Buffer[%d] [ ", next_produced, buffer->in);
            buffer->in = (buffer->in + 1) % num_items;

            printBuffer(buffer);
            printf("]\n");
        }
        else
        {
            fprintf(stdout, "    <Buffer is full>\n");
        }

        /* Crit section completed release the mutex lock*/
        pthread_mutex_unlock(&mutex_lock);

        /* increment full semaphore */
        sem_post(&full); 

    } while(buffer->in < (num_items - 1));
}

void *consumer(void *arg)
{
    bufferStruct *buffer = arg;
    int next_consumed;
    do {        
        /* decrement full semaphore */
        sem_wait(&full);

        /* lock mutex for critical section */
        pthread_mutex_lock(&mutex_lock);

        if (buffer->in != buffer->out)
        {
            next_consumed = buffer->content[buffer->out];
            printf("Consumer: Consumed %d from Buffer[%d] [ ", next_consumed, buffer->out);
            buffer->out = (buffer->out + 1) % num_items;

            printBuffer(buffer);
            printf("]\n");
        }
        else
        {
            fprintf(stdout, "    <Buffer is empty>\n");
        }

        /* Crit section completed release the mutex lock*/
        pthread_mutex_unlock(&mutex_lock);

        /* increment empty semaphore */
        sem_post(&empty);
    } while(buffer->out < (num_items-1));
}


int main(int argc, char *argv[])
{
    static bufferStruct *boundedBuffer;

    if (argc < 2)
    {
        printf("Error, not enough args.\n");
        printf("Usage: ./part1 <num_items_produced>\n");

        return -1;
    }

    num_items = atoi(argv[1]);

    if (num_items > MAX_LENGTH)
        num_items = MAX_LENGTH;

    /* Add one to num items to populate last element in buffer */
    num_items++;

    boundedBuffer = malloc(sizeof(bufferStruct) + sizeof(int)*num_items);

    /* Init mutex lock */
    pthread_mutex_init(&mutex_lock, NULL);

    /* Init full to 0 and empty to MAX_LENGTH */
    sem_init(&full, 0, 0);
    sem_init(&empty, 0, num_items);

    /* Init pthread */
    pthread_t thread[2];

    /* Create producer thread */
    pthread_create(&thread[0], NULL, &producer, boundedBuffer);

    /* Create consumer thread */
    pthread_create(&thread[1], NULL, &consumer, boundedBuffer);

    /* Wait for threads to finish */
    pthread_join(thread[0], NULL);
    pthread_join(thread[1], NULL);

    /* Destroy lock */
    pthread_mutex_destroy(&mutex_lock);

    return 0;
}