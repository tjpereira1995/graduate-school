#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sched.h>

typedef struct {
    struct timespec start;
    struct timespec end;
    float data[];
} TEST_DATA;

double getLatency(struct timespec _start, struct timespec _end)
{
    return (double)( (_end.tv_sec - _start.tv_sec) + (_end.tv_nsec -_start.tv_nsec) * 1e-9);
}

double getTimestamp(struct timespec _time)
{
        return (double)(_time.tv_sec + (_time.tv_nsec));
}

int main(int argc, char *argv[])
{
    int sock_fd, net_fd, _sockopt = 1;
    struct sockaddr_in in;

    int i, size, count, dropped = 0, samples, shm_fd;
    double min = DBL_MAX , max = 0, total =0, delta =6;
    struct timespec start, end, first, last;

    /* Generate flexible array member of struct */
    samples = atoi(argv[1]);
    size = sizeof(TEST_DATA) + sizeof(float) * samples;
    count = atoi(argv[2]);

    /* Set cpu affinity for more consistent results */
    cpu_set_t my_set;
    CPU_ZERO(&my_set);
    CPU_SET(3, &my_set);
    sched_setaffinity(0,sizeof(cpu_set_t),&my_set);
    
    memset(&in, 0, sizeof(in));
    if (fork() == 0)
    {
        TEST_DATA *child_data;
        child_data = malloc(size);
        memset(child_data, 0, size);

        /* Setup socket */
        sock_fd = socket(AF_INET, SOCK_STREAM, 0);
        in.sin_family = AF_INET;
        in.sin_port = htons(1717);
        inet_pton(AF_INET, "127.0.0.1", &in.sin_addr);

        setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &_sockopt, sizeof(int));

        /* Bind to socket */
        if ( bind(sock_fd, (struct sockaddr *)&in, sizeof(in)) < 0)
        {
            perror("bind error");
            return -1;
        }

        /* Listen on socket */
        if ( listen(sock_fd, 128) < 0)
        {
            perror("listen error");
            return -1;
        }

        /* Accept connection to socket */
        if ( (net_fd = accept(sock_fd, NULL, NULL)) < 0)
        {
            perror("accept error");
            return -1;
        }

        for (i = 0; i < count; i++)
        {
            /* Read from socket */
            read(net_fd, child_data, size);

            /* Get end time of write and when data available */
            clock_gettime(CLOCK_REALTIME, &child_data->end);

            /* Check that data written is correct */
            if ( abs(child_data->data[0] - cos(i)) > .0001)
                printf("Wrong Data %lf != %lf\n" , cos(i), child_data->data[0]);

            /* Noticed packets being fropped at some xfer sizes
             * may have to do with MTU size or how packets are
             * fragmented. Account for dropped packets for test
             */
            if ( getTimestamp(child_data->start) <= 0)
            {
                /* Dropped first packet take best guess at start time */
                if (i == 0)
                    first=child_data->end;

                /* Dropped last packet, still capture end time */
                if (i + 1 == count)
                    last = child_data->end;

                dropped++;
            }
            else
            {
                /* Calculate latency */
                delta = getLatency(child_data->start, child_data->end);

                /* Update total writing time */
                total += delta;

                /* Check if first write into shared mem */
                if (i == 0)
                    first=child_data->start;

                /* Check if last write into shared mem */
                if (i + 1 == count)
                    last = child_data->end;

                /* Check for min */
                if (delta < min)
                    min = delta;

                /* Check for max */
                if (delta > max)
                    max = delta;

                //printf("Val: %.041f, Deltä: s.08lf\n", child_data->data[0], delta) ;
            }

        }

        /* Print out test stats */
        printf("\n%.03lf MB/s, Avg Lat: %.05lf us, Min Lat: %.05lf us, Max Lat: %.05lf us [%d packets lost (%.01lf%)]\n" ,
              (size / getLatency(first, last) * 1e-6 * (count - dropped)),
              (total / (count - dropped)) * 1e6, min * 1e6, max * 1e6, dropped, (dropped*1.0/count)*100 );

        free(child_data);
    }
    else
    {
        TEST_DATA *data;

        data = malloc(size);
        memset(data, 0, size);

        sleep(1);

        /* Setup socket */
        sock_fd = socket(AF_INET, SOCK_STREAM, 0);
        in.sin_family = AF_INET;
        in.sin_port = htons(1717);
        inet_pton(AF_INET, "127.0.0.1", &in.sin_addr);

        /* Bind to socket */
        if ( connect(sock_fd, (struct sockaddr *)&in, sizeof(in)) < 0)
        {
            perror("connect error");
            return -1;
        }

        /* Start writing to shm */
        for (i = 0; i < count; i++)
        {
            /* Update data */
            data->data[0] = cos(i);
            clock_gettime(CLOCK_REALTIME, &data->start); 

            /* Send msg */
            write(sock_fd, data, size);      
        }

        /* Cleanup */
        free(data);
    }

    return 0;
}
