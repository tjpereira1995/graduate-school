#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sched.h>

typedef struct {
    struct timespec start;
    struct timespec end;
    float data[];
} TEST_DATA;

double getLatency(struct timespec _start, struct timespec _end)
{
    return (double)( (_end.tv_sec - _start.tv_sec) + (_end.tv_nsec -_start.tv_nsec) * 1e-9);
}

int main(int argc, char *argv[])
{
    int i, size, count, samples, shm_fd;
    double min = DBL_MAX , max = 0, total =0, delta =6;
    struct timespec start, end, first, last;
    char name[16] = "TEST_SHM"; shm_unlink(name);
    sem_t *sem1;
    sem_t *sem2;

    /* Generate flexible array member of struct */
    samples = atoi(argv[1]);
    size = sizeof(TEST_DATA) + sizeof(float) * samples;
    count = atoi(argv[2]);

    /* Set cpu affinity for more consistent results */
    cpu_set_t my_set;
    CPU_ZERO(&my_set);
    CPU_SET(3, &my_set);
    sched_setaffinity(0,sizeof(cpu_set_t),&my_set);

    /* Open semaphores */
    sem1 = sem_open("sem1", O_CREAT, 0644, 1);
    sem2 = sem_open("sem2", O_CREAT, 0644, 0);
    
    if (fork() == 0)
    {
        TEST_DATA *child_data;

        /* Wait for shared mem to be created first */
        sem_wait(sem2);
        
        shm_fd = shm_open(name, O_RDWR, 0666);

        if (shm_fd < 0)
        {
            perror("shm_open error");
            return -1;
        }

        if ( (ftruncate(shm_fd, size)) < 0)
        {
            perror("frtruncate error");
            return -1;
        }

        /* Map pointer of struct to shared mem */
        child_data = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);

        /* Post sem to start performance test */
        sem_post(sem1);

        for (i = 0; i < count; i++)
        {
            /* Wait for write int shared mem */
            sem_wait(sem2);

            /* Get end time of write and when data available */
            clock_gettime(CLOCK_REALTIME, &child_data->end);

            /* Check that data written is correct */
            if ( abs(child_data->data[0] - cos(i)) > .0001)
            {
                printf("Wrong Data %lf != %lf\n" , cos(i), child_data->data[0]);
            }

            /* Calcukate latency */
            delta = getLatency(child_data->start, child_data->end);

            /* Update total writing time */
            total += delta;

            /* Check if first write into shared mem */
            if (i == 0)
                first=child_data->start;

            /* Check if last write into shared mem */
            if (i + 1 == count)
                last = child_data->end;

            /* Check for min */
            if (delta < min)
                min = delta;

            /* Check for max */
            if (delta > max)
                max = delta;

            //printf("Val: %.041f, Deltä: s.08lf\n", child_data->data[0], delta) ;
 
            /* Post sem so that parent can write again */
            sem_post(sem1);

        }

        /* Print out test stats */
        printf("\n%.03lf MB/s, Avg Lat: %.05lf us, Min Lat: %.05lf us, Max Lat: %.05lf us\n" ,
              (size / getLatency(first, last) * 1e-6 * count),
              (total / count) * 1e6, min * 1e6, max * 1e6);

        munmap(child_data, size);
    }
    else
    {
        TEST_DATA *data;

        /* Open shared mem */
        shm_fd = shm_open(name, O_RDWR | O_CREAT, 0666);

        if (shm_fd < 0)
        {
            perror("shm_open error");
            return -1;
        }

        if ((ftruncate(shm_fd, size))<0)
        {
            perror("frtruncate error");
            return -1;
        }

        sem_post(sem2);
        sem_wait(sem1);

        /* Map pointer of struct to shared mem */
        data = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);

        /* Start writing to shm */
        for (i = 0; i < count; i++)
        {
            data->data[0] = cos(i);
            clock_gettime(CLOCK_REALTIME, &data->start); 

            /* Signal Child to start */
            sem_post(sem2);

            /* Wait for child to finish*/
            sem_wait(sem1);       
        }

        /* Cleanup */
        munmap(data, size);
        shm_unlink(name);

        sem_close(sem1);
        sem_destroy(sem1);
        sem_close(sem2);
        sem_destroy(sem2);
    }

    return 0;
}
