#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <semaphore.h>
#include <sched.h>

#define DEFAULT_TYPE (1)
#define DEFAULT_KEY  (0xFF)

typedef struct {
    long type;
    struct timespec start;
    struct timespec end;
    float data[];
} msgBuff;

double getLatency(struct timespec _start, struct timespec _end)
{
    return (double)( (_end.tv_sec - _start.tv_sec) + (_end.tv_nsec -_start.tv_nsec) * 1e-9);
}

double getTimestamp(struct timespec _time)
{
    return (double)( _time.tv_sec + (_time.tv_nsec) * 1e-9);
}

void delete_queue(int qid)
{
    msgctl(qid, IPC_RMID, 0);
}

int create_queue()
{
    return msgget(DEFAULT_KEY, IPC_CREAT | 0666);    
}

int get_queue_id()
{
    return msgget(DEFAULT_KEY, 0400);
}

void send_msg(int qid, msgBuff *buffer, int size)
{
    if ((msgsnd(qid, (struct msgBuff *)buffer, size, DEFAULT_TYPE)) < 0)
    {
        perror("msgsnd error");
        exit(-1);
    }
}

void read_msg(int qid, msgBuff *buffer, int size)
{
    if (( msgrcv(qid, (struct msgBuff *)buffer, size, DEFAULT_TYPE, IPC_NOWAIT)) < 0)
    {
        perror("msgrcv error");
        exit(-1);
    }
}

int main(int argc, char *argv[])
{
    int msg_qid;
    key_t key;

    int i, size, count, samples, shm_fd;
    double min = DBL_MAX , max = 0, total =0, delta =6;
    struct timespec start, end, first, last;
    char name[16] = "TEST_SHM"; shm_unlink(name);

    sem_t *sem1;
    sem_t *sem2;

    /* Generate size for flexible array member of struct */
    samples = atoi(argv[1]);
    size = sizeof(msgBuff) + sizeof(float) * samples;
    count = atoi(argv[2]);

    /* Set cpu affinity for more consistent results */
    cpu_set_t my_set;
    CPU_ZERO(&my_set);
    CPU_SET(3, &my_set);
    sched_setaffinity(0,sizeof(cpu_set_t),&my_set);

    /* Open semaphores */
    sem1 = sem_open("sem1", O_CREAT, 0644, 1);
    sem2 = sem_open("sem2", O_CREAT, 0644, 0);
    
    if (fork() == 0)
    {
        msgBuff *child_data;
        child_data = malloc(size);
        memset(child_data, 0, size);

        child_data->type = DEFAULT_TYPE;

        /* Wait for msg queue to be created first */
        sem_wait(sem2);
        
        msg_qid = get_queue_id();

        /* Post sem to start performance test */
        sem_post(sem1);

        for (i = 0; i < count; i++)
        {
            /* Wait for write int shared mem */
            sem_wait(sem2);

            /* Read msg */
            read_msg(msg_qid, child_data, size);

            /* Get end time of write and when data available */
            clock_gettime(CLOCK_REALTIME, &child_data->end);

            /* Check that data written is correct */
            if ( abs(child_data->data[0] - cos(i)) > .0001)
                printf("Wrong Data %lf != %lf\n" , cos(i), child_data->data[0]);

            /* Calculate latency */
            delta = getLatency(child_data->start, child_data->end);

            /* Update total writing time */
            total += delta;

            /* Check if first write into shared mem */
            if (i == 0)
                first=child_data->start;

            /* Check if last write into shared mem */
            if (i + 1 == count)
                last = child_data->end;

            /* Check for min */
            if (delta < min)
                min = delta;

            /* Check for max */
            if (delta > max)
                max = delta;

            //printf("Val: %.041f, Deltä: s.08lf\n", child_data->data[0], delta) ;
 
            /* Post sem so that parent can write again */
            sem_post(sem1);
        }

        /* Print out test stats */
        printf("\n%.03lf MB/s, Avg Lat: %.05lf us, Min Lat: %.05lf us, Max Lat: %.05lf us\n" ,
              (size / getLatency(first, last) * 1e-6 * count),
              (total / count) * 1e6, min * 1e6, max * 1e6);

        free(child_data);
    }
    else
    {
        msgBuff *data;
        data = malloc(size);
        memset(data, 0, size);

        data->type = DEFAULT_TYPE;

        /* Create msg queue */
        msg_qid = create_queue();

        /* Post sem for child */
        sem_post(sem2);
        sem_wait(sem1);

        /* Start writing to msg_queue */
        for (i = 0; i < count; i++)
        {
            /* Update data */
            data->data[0] = cos(i);
            clock_gettime(CLOCK_REALTIME, &data->start); 

            /* Send msg */
            send_msg(msg_qid, data, size);

            /* Signal Child to start */
            sem_post(sem2);

            /* Wait for child to finish*/
            sem_wait(sem1);       
        }

        /* Cleanup */
        free(data);
        delete_queue(msg_qid);

        sem_close(sem1);
        sem_destroy(sem1);
        sem_close(sem2);
        sem_destroy(sem2);
    }

    return 0;
}