#!/usr/bin/env python

import os
import time
from subprocess import PIPE, Popen
import matplotlib.pyplot as matplot
import pandas as pd

def simple_bar_plot(x, y, test_name):
    matplot.bar(x,y)
    matplot.xlabel("ipc_size [bytes]")
    matplot.ylabel("latency [us]")
    matplot.xticks(range(len(x)), x)
    matplot.title(test_name)

    matplot.savefig("Results/" + test_name + ".png")
    matplot.clf()

def grouped_bar_plot(x, y1, y2, y3, y4, y1_name, y2_name, y3_name, y4_name):
    df_list = []

    # Grab data and put in DataFrame format
    for i in range(0, len(x)):
        df_list.append([y1_name, int(x[i]), y1[i]])

    for i in range(0, len(x)):
        df_list.append([y2_name, int(x[i]), y2[i]])

    for i in range(0, len(x)):
        df_list.append([y3_name, int(x[i]), y3[i]])

    for i in range(0, len(x)):
        df_list.append([y4_name, int(x[i]), y4[i]])

    df = pd.DataFrame(df_list, columns=['ipc_test','ipc_size','latency']) 

    df.pivot("ipc_size","ipc_test","latency").plot(kind="bar")

    matplot.title("IPC Comparison")
    matplot.ylim(0, 10)
    matplot.savefig("Results/ipc_test_results.png")


num_samples = '1000' # Run tests 10k times

ipc_sizes = ['1', '2', '4', '8', '16', '32', '64', '128', '256', '512', 
             '1024' ] 

tests = ['pipe_exe', 'shm_exe', 'msgq_exe', 'tcp_exe']

# Configure /proc/sys/kernel/msgmax size to 1 MB
os.system("sudo sysctl -w kernel.msgmax=1048576")

# Configure /proc/sys/fs/pipe size to 1MB
os.system("sudo sysctl -w fs.pipe-max-size=1048576")

# Empty list for average lats
avgs = []
pipe_avgs = []
shm_avgs  = []
msgq_avgs = []
tcp_avgs  = []

for test in tests:
    # Reset avg list
    avgs = []

    # Need to reduce sample size for tcp, having issues running on VM
    if 'tcp_exe' in test:
        num_samples = '100'

        # Having weird issue with 256 ipc_size, may have to do w/ MTU
        # reducing to 255 gives valid latency
        ipc_sizes[ipc_sizes.index('256')] = '255'

    for size in ipc_sizes:
        avg_lat = 0.0
        for i in range(0,10):
            result = Popen(['./'+ test, size, num_samples], 
                           stdout=PIPE).communicate()[0]

            avg_lat += float(result.split(",")[1].split(" ")[3])
            print test, size, result

        avgs.append(avg_lat / 10.0)

    # Plot test results
    simple_bar_plot(ipc_sizes, avgs, test.replace("exe", "test"))

    # Save data to sep list
    if "pipe" in test:
        pipe_avgs = avgs
    elif "shm" in test:
        shm_avgs = avgs
    elif "msgq" in test:
        msgq_avgs = avgs
    else:
        tcp_avgs = avgs

    # Sleep for a bit betwen tests
    time.sleep(3)

ipc_sizes[ipc_sizes.index('255')] = '256'

# Plot and compare different ipcs
grouped_bar_plot(ipc_sizes, pipe_avgs, shm_avgs, msgq_avgs, tcp_avgs, 
                 tests[0].replace("exe", "test"), 
                 tests[1].replace("exe", "test"), 
                 tests[2].replace("exe", "test"), 
                 tests[3].replace("exe", "test"))
