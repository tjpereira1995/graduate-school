#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sched.h>

/* EDIT /proc/sys/fs/pipe-max-size */

typedef struct
{
    struct timespec start;
    struct timespec end;
    float data[];
} TEST_DATA;

double getLatency(struct timespec _start, struct timespec _end)
{
    return (double)( (_end.tv_sec - _start.tv_sec) + (_end.tv_nsec -_start.tv_nsec) * 1e-9);
}

double getTimestamp(struct timespec _time)
{
    return (double)( _time.tv_sec + (_time.tv_nsec) * 1e-9);
}
int main(int argc, char *argv[]) {
    int              pipefd[2] = {0};
    int              i, samples, size, count, sum, n;
    char            *buf;
    double max = 0, total = 0, delta = 0;
    double min = DBL_MAX;
    
    struct timespec start, end, first, last;   
    
    sem_t *sem1;
    sem_t *sem2;

    samples = atoi(argv[1]);
    size = sizeof(TEST_DATA) + sizeof(float) * samples;
    count = atoi(argv[2]);
    
    if (pipe(pipefd) == -1)
    {
        perror("pipe");
        return 1;
    }

    if ( fcntl(pipefd[0], F_SETPIPE_SZ, size) < 0)
    {
        perror("set pipe size error");
        return -1;
    }

    if ( fcntl(pipefd[1], F_SETPIPE_SZ, size) < 0)
    {
        perror("set pipe size error");
        return -1;
    }

    /* Set cpu affinity for more consistent results */
    cpu_set_t my_set;
    CPU_ZERO(&my_set);
    CPU_SET(3, &my_set);
    sched_setaffinity(0,sizeof(cpu_set_t),&my_set);

    /* Open semaphores */
    sem1 = sem_open("sem1", O_CREAT, 0644, 1);
    sem2 = sem_open("sem2", O_CREAT, 0644, 0);

    /* Child Code */
    if (fork() == 0)
    {
        /* Wait for parent  */
        sem_wait(sem2);

        TEST_DATA *child_data;
        
        child_data = malloc(size);
        memset(child_data, 0, size);

        /* Post sem to start performance test */
        sem_post(sem1);
        
        for (i = 0; i < count; i++)
        {
            /* Wait for write */
            sem_wait(sem2);

            if ( read(pipefd[0], child_data, size) < 0)
            {
                perror("read error");
            }

            clock_gettime(CLOCK_REALTIME, &child_data->end);

            /* Check that data written is correct */
            if ( abs(child_data->data[0] - cos(i)) > .0001)
            {
                printf("Wrong Data %lf != %lf\n" , cos(i), child_data->data[0]);
            }

            /* Calculate latency */
            delta = getLatency(child_data->start, child_data->end);

            /* Update total writing time */
            total += delta;

            /* Check if first write into shared mem */
            if (i == 0)
                first=child_data->start;

            /* Check if last write into shared mem */
            if (i + 1 == count)
                last = child_data->end;

            /* Check for min */
            if (delta < min)
                min = delta;

            /* Check for max */
            if (delta > max)
                max = delta;

            //printf("Val: %.04lf, Delta: %.08lf\n", child_data->data[0], delta);

            /* Post sem so that parent can write again */
            sem_post(sem1);
        }

        /* Print out test stats */
        printf("\n%.03lf MB/s, Avg Lat: %.05lf us, Min Lat: %.05lf us, Max Lat: %.05lf us\n" ,
              (size / getLatency(first, last) * 1e-6 * count),
              (total / count) * 1e6, min * 1e6, max * 1e6);

        free(child_data);
    }
    else
    {
        TEST_DATA *data;
        
        data = malloc(size);
        memset(data, 0, size);

        sem_post(sem2);
        sem_wait(sem1);
        
        for (i = 0; i < count; i++)
        {
            data->data[0] = cos( i );
            clock_gettime(CLOCK_REALTIME, &data->start);
            if (write(pipefd[1], data, size) != size)
            {
                perror("write error");
                return 1;
            }

            /* Signal Child to start */
            sem_post(sem2);

            /* Wait for child to finish*/
            sem_wait(sem1);     
        }

        free(data);
        sem_close(sem1);
        sem_destroy(sem1);
        sem_close(sem2);
        sem_destroy(sem2);
    }
    return 0;
}
