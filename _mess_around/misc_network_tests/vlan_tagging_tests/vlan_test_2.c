/***
 *
 * Tyler Pereira
 * VLAN Tagging Effort
 *
 ***/

#include <linux/if_packet.h>
#include <linux/if_ether.h> 
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <elf.h>
#include <fcntl.h>

/* GLOBALS */

#define VLAN        True      // Make an arg
#define PACKET_SIZE 2048     // Make an arg
#define IFNAME      "eth0" // Make an arg

/* 0     48|     96|      128| 144|            176| 
 * +-------+-------+---------+----+---------------+
 * | DMAC  | SMAC  |8100 VLAN|Type|Payload (4Bfix)|
 * +-------+-------+---------+----+---------------+
 *                  <-------> when VLAN == Yes
 */
struct EthernetHeader {
	uint32_t dest_mac_lo;
	uint16_t dest_mac_hi;
	uint32_t src_mac_lo;
	uint16_t src_mac_hi;
#ifdef VLAN
	uint32_t vlan_tag;
#endif
	uint16_t type;
	char  *payload;
};

struct EthernetHeader2 {
	unsigned char dest[ETH_ALEN];
	unsigned char src[ETH_ALEN];
#ifdef VLAN
	uint32_t vlan_tag;
#endif
	uint16_t type;
	char  *payload;
};



/**
 * Open a socket for the network interface
 */
int32_t open_socket(uint32_t *index) {
  
	char buf[PACKET_SIZE];
	char if_name[] = IFNAME;
	struct ifreq if_ref;
	struct sockaddr_ll sll;
	uint32_t if_index;
  
	int sock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if (sock == -1) 
	{
		printf("%s - Failed to open socket\n", if_name);
		exit(-1);
	}

	// get interface index
	memset(&if_ref, 0, sizeof(if_ref));
	strncpy(if_ref.ifr_name, if_name, strlen(if_name));

	if (ioctl(sock, SIOCGIFINDEX, &if_ref) == -1) 
	{
		printf("%s - Could not get index\n", if_name);
		exit(-1);
	}

	if_index = if_ref.ifr_ifindex;
	*index = if_index;

	memset(&sll, 0xff, sizeof(sll));
	sll.sll_family = AF_PACKET;
	sll.sll_protocol = htons(ETH_P_ALL);
	sll.sll_ifindex = if_index;
  
	if (bind(sock, (struct sockaddr *)&sll, sizeof(sll)) == -1) 
	{
		printf("%s - Failed to bind to sock sll addr\n", if_name);
		exit(-1);
	}
	return sock;
}


void mac_to_int(char mac[], uint32_t *lo, uint16_t *hi){
	/* Need to make copy of param */
	char tmp[strlen(mac)];
	strcpy(tmp, mac);

	/* Split mac address copied to tmp */
	int i = 0;
	char *mac_hex[6];
	char *splitter = strtok(tmp, ":");
	while (splitter != NULL)
	{
		mac_hex[i++] = splitter;
		splitter = strtok(NULL, ":");
	}

	/* Convert mac hex string array to int */
	int shift = 0;
	uint8_t x = 0;
	for (i=5; i >=2; i--)
	{
		x = (uint8_t)strtol(mac_hex[i], NULL, 16);
		*lo = *lo + ((uint32_t)x << shift++*8);
	}
	/* Get high 16 bits */
	shift = 0;
	for (i=1; i >=0; i--)
	{
		x = (uint8_t)strtol(mac_hex[i], NULL, 16);
		*hi = *hi + ((uint16_t)x << shift++*8);
	}

}

void build_packet(struct EthernetHeader *packet, uint32_t dest_lo, uint16_t dest_hi, uint32_t src_lo, uint16_t src_hi, uint16_t type, uint32_t vlan_tag,char *payload) {

	packet->dest_mac_lo = dest_lo;
	packet->dest_mac_hi = dest_hi;

	packet->src_mac_lo = src_lo;
	packet->src_mac_hi = src_hi;

	#ifdef VLAN
	packet->vlan_tag = vlan_tag;
	#endif

	packet->type = type;
	packet->payload = payload;
}

/*
int main(){
	uint32_t if_index;
	uint32_t dest_mac_lo;
	uint16_t dest_mac_hi;
	uint32_t src_mac_lo;
	uint16_t src_mac_hi;

	struct EthernetHeader packet;

	int sock = open_socket(&if_index);

	mac_to_int(DEST_MAC, &dest_mac_lo, &dest_mac_hi);
	mac_to_int(SRC_MAC, &src_mac_lo, &src_mac_hi);

	build_packet(&packet, dest_mac_lo, dest_mac_hi, src_mac_lo, src_mac_hi, ETH_P_IP, 1, "Hello World!"); 
	
	printf("Packet Dest Mac: %x%x\n", packet.dest_mac_hi, packet.dest_mac_lo);
	printf("Packet Src Mac:  %x%x\n", packet.src_mac_hi, packet.src_mac_lo);
	printf("Packet VLAN Tag: %x\n", packet.vlan_tag);
	printf("Packet Type:     %x\n", packet.type);
	printf("Packet Payload:  %s\n", packet.payload);

   unsigned char dest[ETH_ALEN]
           = { 0x98, 0xE7, 0x43, 0xE7, 0x33, 0xC4 };
	struct sockaddr_ll saddrll;
	memset((void*)&saddrll, 0, sizeof(saddrll));
	saddrll.sll_family = PF_PACKET;   
	saddrll.sll_ifindex = 2;
	saddrll.sll_halen = ETH_ALEN;
	memcpy((void*)(saddrll.sll_addr), (void*)dest, ETH_ALEN);

	if (sendto(sock, (void *)packet.payload, sizeof(packet), 0, 
		(struct sockaddr *)&saddrll, sizeof(saddrll)) > 0 )
		printf("Success\n");
	else
		printf("Failed\n");

	printf("End\n");
	return 0;
}*/



int main(){

	unsigned char src[ETH_ALEN]  = { 0x08, 0x00, 0x27, 0x10, 0xEA, 0x44 };
	unsigned char dest[ETH_ALEN] = { 0x98, 0xE7, 0x43, 0xE7, 0x33, 0xC4 };
	struct EthernetHeader2 packet;

	packet.dest = dest;
	packet.src = src;
	#ifdef VLAN
	packet.vlan_tag = 1;
	#endif
	packet.type = ETH_P_IP;
	packet.payload = "Hello World!";

	return 0;
}
