/***
 *
 * Tyler Pereira
 * VLAN Tagging Effort
 *
 ***/

#include <linux/if_packet.h>
#include <linux/if_ether.h> 
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <elf.h>
#include <fcntl.h>

/* GLOBALS */

#define VLAN        True      // Make an arg
#define PACKET_SIZE 2048     // Make an arg
#define IFNAME      "eth0" // Make an arg
#define PORT 		8080

struct __attribute__((packed)) EthernetHeader {
	uint8_t dest[ETH_ALEN];
	uint8_t  src[ETH_ALEN];
	uint16_t type;
#ifdef VLAN
	uint32_t vlan_tag;
#endif
	char  *payload;
};


/**
 * Open a socket for the network interface
 */
int32_t open_socket(uint32_t *index) {
  
	char buf[PACKET_SIZE];
	char if_name[] = IFNAME;
	struct ifreq if_ref;
	struct sockaddr_ll sll;
	uint32_t if_index;
  
	int sock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if (sock == -1) 
	{
		printf("%s - Failed to open socket\n", if_name);
		exit(-1);
	}

	// get interface index
	memset(&if_ref, 0, sizeof(if_ref));
	strncpy(if_ref.ifr_name, if_name, strlen(if_name));

	if (ioctl(sock, SIOCGIFINDEX, &if_ref) == -1) 
	{
		printf("%s - Could not get index\n", if_name);
		exit(-1);
	}

	if_index = if_ref.ifr_ifindex;
	*index = if_index;

	memset(&sll, 0xff, sizeof(sll));
	sll.sll_family = AF_PACKET;
	sll.sll_protocol = htons(ETH_P_ALL);
	sll.sll_ifindex = if_index;
  
	if (bind(sock, (struct sockaddr *)&sll, sizeof(sll)) == -1) 
	{
		printf("%s - Failed to bind to sock sll addr\n", if_name);
		exit(-1);
	}
	return sock;
}

void build_packet(struct EthernetHeader *packet, uint8_t dest[], uint8_t src[], uint16_t type, uint32_t vlan_tag,char *payload) {

	memcpy(packet->dest, dest, sizeof(dest));
	memcpy(packet->src, src, sizeof(src));

	#ifdef VLAN
	packet->vlan_tag = vlan_tag;
	#endif

	packet->type = type;
	packet->payload = payload;
}

int main(){
	uint32_t if_index;

	uint8_t src[ETH_ALEN]   = {0x08,0x00,0x27,0x10,0xEA,0x44};
	uint8_t dest[ETH_ALEN]  = {0x98,0xE7,0x43,0xE7,0x33,0xC4};

	struct EthernetHeader packet;

	int sock = open_socket(&if_index);

	build_packet(&packet, dest, src, ETH_P_8021Q, 1, "Hello World!"); 
	
	printf("Packet Dest Mac: %x\n", packet.dest[0]);
	printf("Packet Src Mac:  %x\n", packet.src[0]);
	printf("Packet VLAN Tag: %x\n", packet.vlan_tag);
	printf("Packet Type:     %x\n", packet.type);
	printf("Packet Payload:  %s\n", packet.payload);

	struct sockaddr_ll saddrll;
	memset((void*)&saddrll, 0, sizeof(saddrll));
	saddrll.sll_family = PF_PACKET;   
	saddrll.sll_ifindex = 2;
	saddrll.sll_halen = ETH_ALEN;
	memcpy((void*)(saddrll.sll_addr), (void*)dest, ETH_ALEN);

	if (sendto(sock, (void *)packet.payload, sizeof(packet), 0, 
		(struct sockaddr *)&saddrll, sizeof(saddrll)) > 0 )
		printf("Success\n");
	else
		printf("Failed\n");

	printf("End\n");
	return 0;
}
